from packages import *

from parameters import *

from meteo_functions import *

from remote_sensing_functions import *

def get_tir_correction(tair_ind:pd.DataFrame,
                       
                       time_rsdata:pd.DataFrame, 
                       
                       rs_data:pd.DataFrame,
                       
                       points:pd.DataFrame,
                       
                       xf:xr.DataArray,yf:xr.DataArray,
                       
                       safran_ws:xr.Dataset,safran_q:xr.Dataset,
                       
                       date:str,hour:int,minute:int,sat:str,tile,
                       
                       ncpu:int,
                       
                       eb_params:dict,
                       
                       wet_edge_comp:bool=False,
                       
                       col_ta:str='ta_d',
                                              
                       x_safran:str='x',y_safran:str='y',time_safran:str='time',
                       
                       name_product_q:str='product',name_product_ws:str='product',
                       
                       window_size:int=3,window_size_tir:int=3):
    
        ''' correction of tir/lst for topographic effects 

        Parameters
        ----------
        tair_ind:pd.DataFrame
            air temperature [K] dataframe per pixel at satellite time overpass and corrected from elevation. 
            Must contain attributes ind and col_ta
        time_rsdata:pd.DataFrame
            time_rsdata : pd.DataFrame
            Metadata of satellite overpass per date, hour, min, tile, sat 
        rs_data:pd.DataFrame
            RS data at given date/sat/tile. Must contain : ind, tir, nir, red, reflectance quality flag (qa_pixel), date, tile, sat
        points : pd.DataFrame
            coordinates dataframe containing ind (unique points id), x_i and y_i coordinates of each points (/!\ regular grid) in tile crs,
            x and y coordinates in safran_crs, lon and lat, longitude and latitude, elevation [m], slope [deg] and aspect[deg]
        
        xf: xr.DataArray corresponding to coordinates longitude / x in safran_crs to interpolate 
    
        yf: xr.DataArray corresponding to coordinates latitude / y in safran_crs to interpolate
        
        safran_ws : xr.Dataset 
            reanalysis wind speed dataset in [ms^-1] per hour
        safran_q : xr.Dataset 
            reanalysis specific humidity dataset in ms^-1 per hour [kg.kg^-1] 
        date : str 
            date YYYY-MM-DD
        hour : int 
            hour
        minute : int 
            minute
        eb_params:dict 
            energy balance paramters 
        ncpu : int
            number of cpu units
        wet_edge_comp:bool
            if false takes col as wet edge (soil and veg) else computes it from EB 
        col_ta : str
            name of the air temperature column in tair
        x_safran : str
            name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
        y_safran : str
            name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files   
        time_safran:str
            name of the coordinate/dimension corresponding to time in .nc reanalysis files 
        name_product_ws : str
            name of the wind speed product in reanalysis .nc files
        name_product_q : str
            name of the q product in reanalysis .nc files
        window_size:int
            size of the window to smooth red, nir values
        window_size_tir:int
            size of the window to smooth lst/tir values 

        Returns
        -------
        dates_ok : pd.DataFrame
            ind, date,hour,minute,sat,tile
            tir_c : float
                corrected land surface temperature from topographic effects [K]
            

        References
        ----------
        * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 
 
        '''
        # name of the air column 
        col_ta='ta_d'
        
        subpoints=points.merge(tair_ind,on='ind')

        # get rg by samani
        subpoints['rg']=get_rg_samani(subpoints['lon'], subpoints['lat'],subpoints['elevation'],subpoints['aspect'],subpoints['slope'],
                                   pd.to_datetime(date),hour=hour,minute=minute)

        # get wind speed and reltive humidity 
        if minute<10 :
            dtime=np.datetime64(date+"T"+str(hour)+":0"+str(minute)+":00")
        else :
            dtime=np.datetime64(date+"T"+str(hour)+":"+str(minute)+":00")
        

        ws=interpol_param_safran(safran_ws,xf,yf,dtime,"ua",
                                 x_safran=x_safran,y_safran=y_safran,time_safran=time_safran,name_product=name_product_ws)
        print("---> Wind speed interpollated ")

        q=interpol_param_safran(safran_q,xf,yf,dtime,"q",
                                x_safran=x_safran,y_safran=y_safran,time_safran=time_safran,name_product=name_product_q)
        print("---> Specific humidity interpollated ")

        subpoints=subpoints.merge(ws, on=[x_safran,y_safran]).merge(q,on=[x_safran,y_safran])
        
        subpoints['ha']=qair2rh(subpoints[col_ta],subpoints['q'])

        del ws,q
        
        print('---> Initial number of pixels :',len(rs_data))
        
        if window_size>1 :
        
            # merge with points 

            rs_data=rs_data.merge(points[['ind','x_i','y_i']],on='ind')

            # smooth bands 

            smooth_red=split_window_average(rs_data[['x_i','y_i','red','date']],
                                            value='red',x="x_i",y="y_i",time="date",
                                            x_size=window_size,y_size=window_size)

            #smooth_red.rename(columns={'red':'red_s'},inplace=True)

            smooth_nir=split_window_average(rs_data[['x_i','y_i','nir','date']],
                                            value='nir',x="x_i",y="y_i",time="date",
                                            x_size=window_size,
                                            y_size=window_size)
            
            rs_data=rs_data.drop(columns=['red', 'nir'])
            
            rs_data=rs_data.merge(smooth_red[['x_i',"y_i",'red']],on=['x_i','y_i'])

            rs_data=rs_data.merge(smooth_nir[['x_i',"y_i",'nir']],on=['x_i','y_i'])
            
            del smooth_nir,smooth_red

            #smooth_nir.rename(columns={'nir':'nir_s'},inplace=True)
            
        if window_size_tir>1 :
            
            smooth_tir=split_window_average(rs_data[['x_i','y_i','tir','date']],
                                            value='tir',x="x_i",y="y_i",time="date",
                                            x_size=window_size_tir,
                                            y_size=window_size_tir)
            rs_data=rs_data.drop(columns=[ 'tir'])

            rs_data=rs_data.merge(smooth_tir[['x_i',"y_i",'tir']],on=['x_i','y_i'])

            del smooth_tir


        # compute ndvi and vindex
        rs_data=fvg_computation(rs_data,sat=sat)
        
        # remove no data in column 
        rs_data.dropna(subset=['tir','vindex'],inplace=True)
        
        #print('Remaining points :',len(rs_data))

        #print("--- Remote sensing data loaded")
        print("---> Selected number of pixels :"+ str(len(rs_data)))


        # optimisation par pixel 
        print("\n---> Per pixel tir simulation ... ")

        tir_sim_data,dta,fss_opt,fsv_opt=optim_tir_sim(subpoints,rs_data,
                                                       ncpu=ncpu,
                                                       wet_edge_comp=wet_edge_comp,
                                                       col=col_ta,
                                                       eb_params=eb_params,)

        # Mean tir simulation 
        print("\n---> Mean tir simulation ... ")
        
        subpoints_mean=pd.DataFrame(subpoints.apply(lambda x : x.mean())).transpose()

        tsd_m,tsw_m,tvd_m,tvw_m=function_endmembers(subpoints_mean,
                                                    dta=dta,
                                                    ncpu=ncpu,
                                                    wet_edge_comp=wet_edge_comp,
                                                    col=col_ta,
                                                    **eb_params)

        # Energy balance repartition
        tir_alts = fss_opt * tsd_m + (1-fss_opt) * tsw_m
        
        tir_altv = fsv_opt * tvd_m + (1-fsv_opt) *tvw_m

        tir_sim_data['tir_alt']= tir_sim_data['vindex'].apply(lambda x : (1-x)*tir_alts+x*tir_altv)

        tir_alt_m = tir_sim_data['tir_alt'].mean()

        tir_mean=tir_sim_data['tir'].mean()

        tir_sim_data['tir_alt2']=tir_sim_data['tir_alt']+(tir_mean-tir_alt_m)

        tir_sim_data['tir_c']=(tir_sim_data['tir']-tir_sim_data['tir_sim'])+tir_sim_data['tir_alt2']

        # add attributes
        tir_sim_data['tile']=tile
        tir_sim_data['sat']=sat
        tir_sim_data['date']=date
        tir_sim_data['minute']=minute
        tir_sim_data['hour']=hour
        
        col2save=['ind','tir_c','date','hour','minute','sat','tile']
        
        return tir_sim_data[col2save]
    
def optim_tir_sim(subpoints:pd.DataFrame,
                  rs_data:pd.DataFrame,
                  ncpu:int,
                  wet_edge_comp:bool,
                  col:str,
                  eb_params:dict):
    
    ''' compute optimal simulated surface temperature per pixel 

    Parameters
    ----------
    subpoints : pd.DataFrame
        DataFrame that contains at least rg , ua, col, ha, elevation and ind attributes 
    
    subpoints : pd.DataFrame
        DataFrame that contains at least tir, vindex, and ind attributes 
    
    col : str 
        attribute name of air temperature in df
    
    ncpu : int
        number of cpu units
    
    eb_params:dict 
            energy balance parameters 

    wet_edge_comp:bool
        if false takes col as wet edge (soil and veg) else computes it from EB 
   
        
    * args : see newtoneb
    
    Returns
    -------
    df : pd.Daframe
        ind : int
            unique id of points/pixels
        fss : float
            optimal fss parameter
        fsv : float
            optimal fsv parameters
        tir_sim : float
            simulated surface temperature [K]
            
    dta : float 
        lapse rate [K.m^-1]
        
    fss_opt,fsv_opt : float,float
        optimal paramters of simulation
        
    References
    ----------
    * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 

    '''
    data = rs_data[['ind','vindex','tir']].set_index('ind').merge(subpoints[["ind","elevation",col]].set_index('ind'),left_index=True,right_index=True).reset_index()
    
    # evaluation of lapse rate
    regressor = LinearRegression()
    regressor.fit(data['elevation'].to_numpy().reshape(-1, 1) , 
                        data[col].to_numpy().reshape(-1, 1))
    
    dta=min(regressor.coef_[0][0],0)
    
    print("Lapse rate :",np.round(dta,5),'[K.m^-1]')

    del regressor
    
    
    # endmembers evaluation of tir-fvg space per pixel
    tsd,tsw,tvd,tvw =function_endmembers(subpoints.set_index('ind'), 
                                         dta=dta,col=col,
                                         wet_edge_comp=wet_edge_comp,ncpu=ncpu,
                                         **eb_params)
    
    data=data.merge(tsd.reset_index(),on='ind')
    
    if wet_edge_comp :
        data=data.merge(tsw.reset_index(),on='ind')
    else :
        data['tsw']=tsw
        
    data=data.merge(tvd.reset_index(),on='ind')
    
    if wet_edge_comp :
        data=data.merge(tvw.reset_index(),on='ind')
    else :
        data['tvw']=tsw
        
    del tsd,tsw,tvd,tvw
    
    print("Number of values all data: ", len(data))
    
    print("Number of values all data without na : ", len(data.dropna()))
    
    data.dropna(inplace=True)

    # optimization of fss and fsv 
    
    rmse_s=compute_tir_sim(data)
    
    rmse_s=rmse_s.groupby(['fss', 'fsv']).apply(rmse_fvdiff_solveg,fvdiff_solveg=fvdiff_solveg)
    
    fss_opt,fsv_opt=rmse_s.loc[rmse_s==min(rmse_s)].index[0]

    print("Global fss_opt="+str(fss_opt)+" / " + "fsv_opt="+str(fsv_opt))

    rmse_s=compute_tir_sim(data,inter_fss=[max(0,fss_opt-0.1),min(1,fss_opt+0.1)],
                            inter_fsv=[max(0,fsv_opt-0.1),min(1,fsv_opt+0.1)],
                            num_fs=11)
    
    rmse_s=rmse_s.groupby(['fss', 'fsv']).apply(rmse_fvdiff_solveg,fvdiff_solveg=fvdiff_solveg)
    
    fss_opt,fsv_opt=rmse_s.loc[rmse_s==min(rmse_s)].index[0]
        
    print("Local fss_opt="+str(fss_opt)+" / " + "fsv_opt="+str(fsv_opt))

    df=compute_tir_sim(data,
                        inter_fss=[fss_opt,fss_opt], 
                        inter_fsv=[fsv_opt,fsv_opt],
                        num_fs=1)
    
    return df,dta,fss_opt,fsv_opt


# function that return extremal temperature tsd,tsw,tvd,tvw
def function_endmembers(df:pd.DataFrame,
                        dta:float,
                        ncpu:int,
                        wet_edge_comp:bool,
                        col:str,
                        kb:dict,albedo:dict,emi:dict,
                        rs:float,h:float,
                        z:float,cg:float,karman:float, rcp:float,
                        sigma:float,grav:float,gam:float):
    
    ''' Per pixel evaluation of tir/lst endmembers of lst-fvg / tir-vindex space

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame that contains at least rg , ua, col, ha, elevation and ind attributes 
    
    dta : float
        lapse rate [k.m^-1]
    
    col : str 
        attribute name of air temperature in df
    
    wet_edge_comp:bool
        if false takes col as wet edge (soil and veg) else computes it from EB  
    
    ncpu : int
        number of cpu units
        
    ** args : see newtoneb
    
    Returns
    -------
    tsd : pd.Serie
        per pixel dry soil surface temperature [K] / index is ind 
    tsw : pd.Serie
        per pixel wet soil surface temperature [K] / index is ind 
    tvd : pd.Serie
        per pixel dry vegetation surface temperature [K] / index is ind 
    tvw : pd.Serie
        per pixel wet vegetation surface temperature [K] / index is ind 
        
    References
    ----------
    * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 
 
    '''
    
    ddf=dd.from_pandas(df[['rg','ua',col,'ha','elevation']].copy(), npartitions=ncpu)
    
    #if(dta!=0):
    #    ddf[col] = ddf[col] + (dta *(ddf["elevation"]-np.mean(ddf["elevation"]))) 
        
    
    # Soil dry endmembers 
    wet, veg = 0, 0
    tsd=ddf.map_partitions(newtoneb_par,
                           col=col,
                           wet=wet,veg=veg,
                           kb=kb[str(veg)],albedo=albedo[str(veg)],emi=emi[str(veg)],
                           rs=rs,h=h,
                           z=z,cg=cg,karman=karman,rcp=rcp,
                           sigma=sigma,grav=grav,gam=gam,
                           meta= pd.Series(dtype="float64",name='tsd')).compute()

    print("Tsd mean [K]: ",tsd.mean())
    
    # Soil wet endmembers 
    if wet_edge_comp==True :
        wet, veg = 1, 0
        tsw=ddf.map_partitions(newtoneb_par,
                               col=col,
                               wet=wet,veg=veg,
                               kb=kb[str(veg)],albedo=albedo[str(veg)],emi=emi[str(veg)],
                               rs=rs,h=h,
                               z=z,cg=cg,karman=karman,rcp=rcp,
                               sigma=sigma,grav=grav,gam=gam,
                               meta= pd.Series(dtype="float64",name='tsw')).compute()
        
    else :
        tsw=df[col].mean()
    
    print("Tsw mean [K] :", tsw.mean())
    
    # vegetation dry endmembers 
    wet, veg = 0, 1
    tvd=ddf.map_partitions(newtoneb_par,
                           col=col,
                           wet=wet,veg=veg,
                           kb=kb[str(veg)],albedo=albedo[str(veg)],emi=emi[str(veg)],
                           rs=rs,h=h,
                           z=z,
                           cg=cg,karman=karman,rcp=rcp,
                           sigma=sigma,grav=grav,gam=gam,
                           meta= pd.Series(dtype="float64",name='tvd')).compute()
    print("Tvd mean [K] :", tvd.mean())
    
    # vegetation wet endmembers 
    if wet_edge_comp==True :
        wet, veg = 1, 1
        tvw=ddf.map_partitions(newtoneb_par,
                               col=col,
                               wet=wet,veg=veg,
                               kb=kb[str(veg)],albedo=albedo[str(veg)],emi=emi[str(veg)],
                               rs=rs,h=h,
                               z=z,
                               cg=cg,karman=karman,rcp=rcp,
                               sigma=sigma,grav=grav,gam=gam,
                               meta= pd.Series(dtype="float64",name='tvw')).compute()
    else :
        tvw=df[col].mean()
        
    print("Tvw mean [K] :", tvw.mean())
    
    return tsd,tsw,tvd,tvw 


def newtoneb_par(df:pd.DataFrame,
                 col:str,
                 wet:bool,veg:bool,
                 kb:float,albedo:float,emi:float,
                 rs:float,h:float,
                 z:float,
                 cg:float,karman:float, rcp:float,sigma:float,
                 grav:float,gam:float):
    
    '''Parallelized version of newtoneb
    Parameters
    ----------
    df : pd.DataFrame
        DataFrame that contain at least rg, ua, col, ha and ind attributes 
    '''
    
    return df.apply(lambda df :newtoneb(rg=df['rg'], ua=df['ua'],ta=df[col],ha=df['ha'],
                                        wet=wet,veg=veg,
                                        kb=kb,albedo=albedo,emi=emi,
                                        rs=rs,h=h,
                                        z=z,
                                        cg=cg,karman=karman,rcp=rcp,sigma=sigma,
                                        grav=grav,gam=gam),
                    axis=1)#,
                    #meta= pd.Series(dtype="float64"))

def newtoneb(rg:float,ua:float,ta:float,ha:float,
             wet:bool,veg:bool,
             # function  of vegetation
             kb:float,albedo:float,emi:float,
             rs:float,h:float,
             z:float,
             cg:float,karman:float, rcp:float,sigma:float,
             grav:float,gam:float,
             km=1500,e:float=0.01):
    
    ''' energy balance solution

    Parameters
    ----------
    rg : float
        short wave incoming solar radiation [W.m^-2]
    
    ua : float
        wind speed [m.s^-1]
    
    ta : float
        temperature of air [K]
    
    ha : float
        relative humidity [-]
    
    wet : bool 
        0 : dry edge / 1 : wet edge
    
    veg : bool
        0 : bare soil / 1 : full vegetation cover
    
    kb :float 
        kb-1 parameter [-]
        
    rs : float
        stomatal resistance (only if veg==1) [s.m^-1]
        
    albedo : float
        soil/veg albedo [-]
    
    emi : float
        soil/veg emissivity [-]
        
    h : float 
        vegetation height [m]
    
    z : float 
        ua/ha measurement heigth [m]
    
    cg : float
        ratio of net flux stored in soil [-]
    
    karman : float
        Von Karmann constant [-]
    
    rcp : float
        rho*Cp = volumetric mass * specific heat of air [J.K^-1.m^-3]
        
    sigma : float 
        Stephan-Boltzmann constant [J.s^-1.m^-2.K^-4]
    
    grav : float 
        gravitationnal constant [m.s^-1]
        
    gam : float
        psychometric constant [Pa.K^-1]
    
    km : int
        maximum number of step in Newton algorithm
    
    e : float 
        learning rate in Newton algorithm
        
    Returns
    -------
    t : float
        aerodynamic temperature [K]
        
        
    References
    ----------
    * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 
    * B.J. Choudhury, R.J. Reginato, and S.B. Idso. An analysis of infrared temperature observations over wheat and calculation of latent heat flux. Agricultural and Forest Meteorology, 37(1):75–88, 1986, 10.1016/0168-1923(86)90029-8
    * D. Troufleau, J.P. Lhomme, B. Monteny, and A. Vidal. Sensible heat flux and radiometric surface temperature over sparse sahelian vegetation. i. an experimental
analysis of the kb-1 parameter. Journal of Hydrology, 188-189:815–838, 1997. HAPEX-Sahel.

    '''
    
    # actual pressure [Pa]
    ea=ha*611.0*np.exp(17.27*(ta-273.15)/(ta-35.9)) 
    
    # emissivity of air [-]
    emia = 0.553*np.power((ea/100.0),(1/7))
    
    # atmospheric longwave incoming radiation [W.m^-2]
    ra=emia*sigma*np.power(ta,4)
    
    if (veg==1) :
        
        # Momentum roughtness length [m]
        zom=0.07*h
        
        # zero plane displacement [m]
        d=0.3*h
        
    elif (veg==0) :
        
        # Momentum roughtness length [m]
        zom=0.07
        
        # zero plane displacement [m]
        d=0
    
    # aerodynamic resistance in neutral conditions [s.m^-1]
    rah0=np.log((z-d)/zom)*np.log((z-d)/zom)/(ua*(np.power(karman,2)))

    # initialization
    t0=0
    
    t=ta+100
    
    k=0
        
    while (np.abs(t0-t)>0.1) and k<km : 
        
        t0=t
        
        k=k+1
        
        # ridchardson number 
        ri=(grav*z*(t-ta))/((ta)*(np.power(ua,2)))
        
        if t<ta :
            n=2
        elif(t>=ta) : 
            n=0.75
        
        # update aerodynamic resistance [s.m^-1]
        rah=rah0/np.power((1+5*ri),n) 
        
        # net flux [W.m^-2]
        rn=(rg*(1-albedo))+emi*(ra)-emi*(sigma*np.power(t,4))
        
        # velocity [m.s^-1]
        ustar=np.sqrt(ua/rah)
        
        # sensible heat [W.m^-2]
        h=rcp*(t-ta)/(rah+kb/(karman*ustar))
        
        # latent heat [W.m^-2]
        if(wet==1) :
            
            es=611*np.exp(17.27*(t-273.15)/(t-35.9)) # pression vapeur saturante en Pa
            
            if (veg==0) :
                
                le=(rcp/gam)*(es-ea)/(rah+kb/(karman*ustar))
                
            elif(veg==1) :
                
                le=(rcp/gam)*(es-ea)/(rah+kb/(karman*ustar)+rs)

        # Newton algorithm update
        
        t_e=t+e
        
        ri_e=grav*z*(t_e-ta)/((ta)*np.power(ua,2)) # nombre de ridchardson 
        
        if(t_e<ta) :
            
            n=2
            
        elif(t_e>=ta) :
            
            n=0.75

        rah_e=rah0/np.power((1+5*ri_e),n) 

        rn_e=rg*(1-albedo)+emi*(ra)-emi*sigma*(np.power(t_e,4))

        ustar_e=np.sqrt(ua/rah_e)

        h_e=rcp*(t_e-ta)/(rah_e+kb/(karman*ustar_e))

        if(wet==1) :
            
            es_e=611*np.exp(17.27*(t_e-273.15)/(t_e-35.9)) # pression vapeur saturante en Pa
            
            if (veg==0) :
                le_e=(rcp/gam)*(es_e-ea)/(rah_e+kb/(karman*ustar_e))
            elif(veg==1) :
                le_e=(rcp/gam)*(es_e-ea)/(rah_e+kb/(karman*ustar_e)+rs)


        #### optimisation gradient
        if(wet==0) :
            ft=np.power((rn*(1-cg)-h),2)
            ft_e=np.power((rn_e*(1-cg)-h_e),2)
        elif(wet==1):
            ft=np.power((rn*(1-cg)-h-le),2)
            ft_e=np.power((rn_e*(1-cg)-h_e-le_e),2)

        fprim_t=(ft_e-ft)/e
        t=t0-(ft/fprim_t)

    if (k==km) :
        
        return np.nan
    
    else :
        
        return t

def compute_tir_sim(data:pd.DataFrame, inter_fss:list=[0,1], inter_fsv:list=[0,1],num_fs:int=11) :
    
    ''' simulation of surface temperature tir

    Parameters
    ----------
    data : pd.DataFrame
        DataFrame that contains at least tir [k], vindex and ind attributes 
    
    inter_fss : list
        list of min and max values of fss parameter to look for
    
    inter_fsv : list 
        list of min and max values of fsv parameters to look for
    
    num_fs :
        number of value to split [min,max] interval in num_fs-1 equally spaced intervals
    
    Returns
    -------
    data : pd.Daframe
        ind : int
            unique id of points/pixels
        fss : float
            fss parameter
        fsv : float
            fsv parameters
        tir_sim : float
            simulated surface temperature [K]
        
    References
    ----------
    * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 
    '''
    
    tir_mean=data['tir'].mean()
    
    #fss and fsv list of values 
    
    fss_list=pd.DataFrame({"fss":np.linspace(inter_fss[0],inter_fss[1],num_fs)})
    
    fsv_list=pd.DataFrame({"fsv":np.linspace(inter_fsv[0],inter_fsv[1],num_fs)})
    
    fs_list=fss_list.merge( fsv_list,how='cross')

    data=data.merge(fs_list,how='cross')
    
    data['s'] = data['fss'] * data['tsd'] + ( 1 - data['fss']) * data['tsw']
    
    data['v'] = data['fsv'] * data['tvd'] + ( 1 - data['fsv']) * data['tvw']
    
    data['m'] = (1 - data['vindex']) * data['s'] + data['vindex'] * data['v']

    # mean of M
    tir_c_mean=pd.DataFrame({"tir_c_mean":data.groupby(['fss', 'fsv'])['m'].mean()})
    
    data=data.join(tir_c_mean, on=['fss','fsv'])
    
    # LST sim computation
    data["tir_sim"] = (tir_mean - data['tir_c_mean']) + data['m'] 
    
    return data

def rmse_fvdiff_solveg(data:pd.DataFrame,y:str="tir",ypred:str="tir_sim",vindex:str="vindex",fvdiff_solveg:float=fvdiff_solveg):
    
    ''' rmse of simulated temperature / ypred vs measured surface temperature/y 

    Parameters
    ----------
    data : pd.DataFrame
        DataFrame that contains at least tir [k], tir_sim, vindex and ind attributes 
    
    y : str 
        measured value
    
    y_pred : str
        predicted value
    
    vindex : float 
        green fraction vegetation cover
    
    Returns
    -------
    rmse : root mean square error 
    
        
    References
    ----------
    * Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 
    '''
    
    threshold_sol = 0.5 - fvdiff_solveg / 2
    
    threshold_veg = 0.5 + fvdiff_solveg / 2
    
    ypred_df=data[ypred].loc[(data[vindex]<=threshold_sol) | (data[vindex]>=threshold_veg) ]
    
    y_df=data[y].loc[(data[vindex]<=threshold_sol) | (data[vindex]>=threshold_veg)] 
    
    rmse = np.sqrt(mean_squared_error(ypred_df,y_df)) if (sum(data[vindex]<=threshold_sol) > 0 and sum(data[vindex]>=threshold_veg) > 0) else np.nan             
    
    return rmse


