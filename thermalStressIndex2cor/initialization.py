from packages import *

from parameters import *

# creating path to points 
# path to points of study area 
if path2points==None : 
    
    path2points = os.path.join(path2wd,name, "ffid"+str(idfeu),"geo_data",
                               "ffid"+str(idfeu)+"_gridpoints.shp")
    point_file=None
    
    if not os.path.exists(path2points):
        print("/!\ Study area points grid not provided")
        print(path2points)


# path dem of study area 
if path2dem == None :
    path2dem = os.path.join(path2wd,name, "ffid"+str(idfeu),"geo_data",
                            "ffid"+str(idfeu)+"_dem.tiff")
    if not os.path.exists(path2dem):
        print("/!\ DEM not provided")
        path2dem=None

# path to reanalysis data 
if path2safran == None :
    path2safran = os.path.join(path2wd,name, "ffid"+str(idfeu),"safran_data")
    if not os.path.exists(path2safran):
        print("/!\ Reanalysis meteo data folder does not exist")
        
if os.path.exists(path2safran):  
    with fiona.open(os.path.join(path2safran,'safran_gridpoints.shp'),'r') as src:
        safran_crs=pyproj.CRS(src.crs['init']).to_epsg()

# path to ground station data 
if path2sta == None : 
    path2sta = os.path.join(path2wd,name, "ffid"+str(idfeu),"sta_data")
    if not os.path.exists(path2safran):
        print("/!\ Ground station meteo data folder does not exist")

if path2viewangles==None :
    path2viewangles=os.path.join(path2wd,name, "ffid"+str(idfeu),"rs_data","ffid"+str(idfeu)+"_satellite_viewing_angles.tif")
    if not os.path.exists(path2viewangles):
        print("/!\ Satellite view angle file does not exist")


if model_type=="nn" : 
    # neural network 
    from tensorflow.keras.utils import Sequence
    from tensorflow import keras
    from tensorflow.keras import layers
    from sklearn.model_selection import  train_test_split


