import os.path 

# =============================================================================
# SET WD AND NAME OF  PROJECT
# =============================================================================

# path 2 folder containing <name> folder 
path2wd='/media/gbio/T00/test_wdi'

# name of the project 
name='trainingsites'

# number of the study site 
idfeu=2

# =============================================================================
# DASK PARAMETERS 
# =============================================================================

# number of cpu units
ncpu=12

# =============================================================================
# REMOTE SENSING DATA PARAMETERS 
# =============================================================================

# value of quality reflectance mask for clear soil pixel and each satellite product 
clear_soil_pixel={"l7":5440,"l8":21824,"l9":21824}

# value of quality reflectance mask for clear water pixel and each satellite product 
clear_water_pixel={"l7":5504,"l8":21888,"l9":21888}

# value of quality reflectance mask for no data pixel and each satellite product 
fill_no_data={"l7":1,"l8":1,"l9":1}

# max and min hour of the interval of satellite overpass 
start_hour=10
end_hour=11

# x/y size of the split window to rs data before computing tir corrected from topographic effects and WDI
# vis/nir data
window_size=3

# thermal data
window_size_tir=3

# =============================================================================
# METEO REANALYSIS DATA PARAMETERS 
# =============================================================================

# safran_resolution [remote sensing crs units] in meters 
# /!\ remote sensing data crs must be in meters
safran_res=8000

# name of the time coordinate in reanalysis .nc files
time_safran='time'

# name of the lon/x coordinate in reanalysis .nc files
x_safran ='x'

# name of the lat/y coordinate in reanalysis .nc files
y_safran ='y'

# Air_temperature near-surface 2m air temperature [K] valid_range: [200. 350.]
# Prefix of all .nc meteo (reanalysis) files
prefix_product_ta="ForcT"
name_product_ta='product'

# Horizontal wind speed at 10 meters height [m.s^-1] valid_range: [  0. 100.]
# Wind speed prefix of all .nc meteo (reanalysis) files
prefix_product_ws="ForcVu"
name_product_ws='product'

# Near-surface 2m specific humidity [kg.kg^-1] valid_range: [0. 1.]
# Prefix of all .nc meteo (reanalysis) files
prefix_product_q="ForcQ"
name_product_q='product'

# Precipitation rate at surface(liquid) [kg.m^-2.s^-1] valid_range: [ 0. 10.]
# Prefix of all .nc meteo (reanalysis) files
prefix_product_r="ForcPRCP"
name_product_r='product'
# multipicative factor 
fact_rain=3600

# =============================================================================
# TAIR DISAGGREGATION PARAMETERS 
# =============================================================================

# model type 'nn' : dense neural network 
model_type='nn'

# ground stations data 
# Prefix of all air temperature files
prefix_sta="sta"
# name of the column of unique id of each station 
unique_id_sta="number_sta"
# name of the air temperature column
sta_name_product='t'
# name of the date-time column
time_sta='date'
# name of the elevation column 
z_sta="height_sta"

# =============================================================================
# SELECTION OF DATES TO CORRECT 
# =============================================================================

# max and min month of the study period in the year 
min_month=1
max_month=12

# minimum frequence of clear soil/water pixels in the study area 
freq_min_pix=0.85

# minimum of delta temp between min_tir and tair mean [K]
dtemp_min=2.5

# =============================================================================
# LAND SURFACE TEMPERATURE CORRECTION 
# =============================================================================

# energy balance parameters 
# key of dicts : 0 : bare soil / 1 : full vegetation cover

eb_params={
    # kb-1 parameter [-]
    'kb':{'0':6.0,'1':2.0},

    # Soil/veg albedo [-]
    'albedo'  : {'0':0.14,'1':0.18},

    # Soil/veg emissivity [-]
    'emi':{'0':0.96,'1':0.98},

    # Stomatal resistance (only if veg==1) [s.m^-1]
    'rs':60,

    # Vegetation height [m]
    'h':2,

    # ta/ua/ha measurement heigth (from meteo files) [m]
    'z':10,

    # ratio of net flux stored in soil [-]
    'cg':0.315,

    # Von Karmann constant [-]
    'karman':0.41,

    # Volumetric mass (density) of air  * specific heat of air [J.K^-1.m^-3]
    'rcp':1262,

    # Stephan-Boltzmann constant [J.s^-1.m^-2.K^-4]
    'sigma':5.67*10**(-8),

    # Gravitationnal constant / acceleration of gravity [m.s^-1]
    'grav':9.81,

    # psychometric constant [Pa.K^-1]
    'gam':67,
}

# optimization parameter 
fvdiff_solveg=0.8

# =============================================================================
# CAST SHADOW CORRECTION 
# =============================================================================

# use of land surface temperature corrected from topographic effect
use_tir_topo_cor=True

# cast shadow correction 
shadow_correction=True

# ====
# a_self self-calibration parameters 
# ====

# rain timelag [day]
rain_timelag=15

# min and max month for calibration of the shadow correction
min_month_calib=5
max_month_calib=9

# min and max year for calibration of the shadow correction
max_year_calib=2022
min_year_calib=2013

# ====
# b_self method 
# ====

# offset b_self among 'sat_view_zenith' or 'min_solar_zenith' or 'fixed'
b_self_method='sat_view_zenith'#'min_solar_zenith'

# fixed value of b_self (if b_self_method=='fixed')
b_self=None

# zenith angle multiplicative parameters (for landsat 8)
fact_zenith=0.01

# =============================================================================
# DO NOT CHANGE / OTHERS PARAMETERS
# =============================================================================
# path to ground station data 
path2sta = None

# path2safran safran data 2be disaggregated 
path2safran=None

# path to dem 
path2dem=None

# path2points 
import fiona
path2points=None
point_file=None

# raster file of satellite view angles [1st layer : azimuth / 2nd layer : zenith]
path2viewangles=None

# index to correct 
wdi_name='wdi'

# open individually rs files <=> files saved by dates (set as False)
read_from_dask=False

# create training dataset 
create_training=True
































