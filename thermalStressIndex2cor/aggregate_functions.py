from packages import * 
from parameters import *


def dates_selection(datathermal:pd.DataFrame,
                    time_rsdata:pd.DataFrame,tair_disagg:pd.DataFrame,
                    idfeu:int,
                    freq_min_pix:float,
                    dtemp_min:float,
                    start_hour:int, end_hour:int,
                    min_month:int,
                    max_month:int,
                    clear_soil_pixel:dict=clear_soil_pixel,
                    clear_water_pixel:dict=clear_water_pixel,
                    fill_no_data:dict=fill_no_data
                   ):
    
    ''' dates selection to compute wdi / tir / lst correction

    Parameters
    ----------
     datathermal: pd.DataFrame
        DataFrame containing RS data : ind, tir, nir, red, reflectance quality flag (qa_pixel), date, tile, sat
    
    time_rsdata : pd.DataFrame
        Metadata of satellite overpass per date, hour, min, tile, sat 
    
    tair_disagg:pd.DataFrame
        Air temperature (K) disagregated or resampled per data and ind 
    
    idfeu : int 
         unique id of study area
    
    freq_min_pix : float 
        Minimum frequency of clear soil and water pixel among a scene / for datathermal filtering
    
    dtemp_min : float 
        Minimum difference of tempreture between tair mean in the study area and tir/lst minimum [K]
    
    start_hour : int
        minimum hour of satellite overpass / included
    
    end_hour : int
        maximum hour of satellite overpass / included
    
    min_month : int
        minimum month of satellite overpass / included
        
    max_month : int
        maximum month of satellite overpass / include    clear_soil_pixel : dict
    
    clear_soil_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear soil pixels for each satellite type (key)
    
    clear_water_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear water pixels for each satellite type (key)        
    
    fill_no_data :
        dictionnary of value of reflectance_quality_flag for filled pixels for each satellite type (key)
    Returns
    -------
    dates_ok : pd.DataFrame 
        date : str
        hour : int
        minute : int
        tile : str
        sat : str
        freq_clear : float
            frequency of clear soil and water pixel among a scene
        ta_d : float
            mean air tempeatur in the study area [K]
        
        
        
    References
    ----------
    Victor PENOT 
    '''
    # selection of dates based on clear pixel 
    df_npixels=datathermal.map_partitions(freq_pixel_by_date,
                                          clear_soil_pixel=clear_soil_pixel,
                                          clear_water_pixel=clear_water_pixel,
                                          fill_no_data=fill_no_data,
                                          meta=pd.DataFrame({'nstrip':[int],
                                                             'nclear':[int],
                                                             'ntot':[int],
                                                             'freq_clear':[float],
                                                             'sat':[str],
                                                             'tile':[str]})).compute().reset_index()[['date','nstrip','nclear','ntot','freq_clear','sat','tile']]
    
    
    date_clear_pixels=df_npixels.loc[df_npixels['freq_clear']>=freq_min_pix]  

    
    # selection of dates based on hour overpass 
    time_rsdata=time_rsdata.loc[(time_rsdata['hour']>=start_hour) & (time_rsdata['hour']<=end_hour)]
    
    if time_rsdata['sat'].dtype=='int64':
        time_rsdata['sat']=time_rsdata['sat'].apply(lambda x : 'l'+str(x))
        
    if time_rsdata['date'].dtype!='str':
        time_rsdata=time_rsdata.astype({'date': 'str'})
    
    if time_rsdata['tile'].dtype=='int64':
        time_rsdata['tile']=time_rsdata['tile'].apply(lambda x : str(x))       
    
    

        
    # selection of dates based on minimum tir/tair temperature
    tair_mean=tair_disagg.map_partitions(mean_tair_by_date,time_rsdata=time_rsdata,col='ta_d',agg_col="date",
                                          meta=pd.DataFrame({'date':[str],'ta_d':[float]})).compute()
    
    min_tir=datathermal.map_partitions(min_col_by_date,col='tir',
                                          meta=pd.DataFrame({'min_tir':[float],
                                                             'sat':[str],
                                                             'tile':[str]})).compute().reset_index()[['date','min_tir','sat','tile']]
    dtemp_dates=pd.merge(min_tir,tair_mean, on='date')
    

    dtemp_dates=dtemp_dates.loc[(dtemp_dates['min_tir']-dtemp_dates['ta_d'])>=-dtemp_min]

    # intersection of dates 
    dates_ok=time_rsdata[['date','sat','tile','hour','minute']].merge(date_clear_pixels[["date","freq_clear","tile","sat"]],on=['date','tile','sat'])

    dates_ok=dates_ok.merge(dtemp_dates[['date','min_tir','ta_d','sat','tile']],on=['date','tile','sat'])
    
    dates_ok['date_datetime']=pd.to_datetime(dates_ok['date'])
    
    dates_ok=dates_ok.loc[(dates_ok['date_datetime'].dt.month>=min_month) & (dates_ok['date_datetime'].dt.month<=max_month) ]
    
    
    return dates_ok

def freq_pixel(df:pd.DataFrame,
               clear_soil_pixel:dict=clear_soil_pixel,
               clear_water_pixel:dict=clear_water_pixel,
               fill_no_data:dict=fill_no_data,
               reflectance_quality_flag:str='qa_pixel'):
    
    ''' evaluate frequency of clear pixel at a given location and date 

    Parameters
    ----------
    df : pd.DataFrame
        dataframe at a given date-hour containing rsdata per pixel, particularly reflectance_quality_flag, sat (satellite) and tile column
    
    clear_soil_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear soil pixels for each satellite type (key)
    
    clear_water_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear water pixels for each satellite type (key)
        
    fill_no_data :
        dictionnary of value of reflectance_quality_flag for filled pixels for each satellite type (key)
        
    reflectance_quality_flag : str
        column name in df of reflectance quality flag 
    
    
    Returns
    -------
    pd.DataFrame : 
        nstrip : int 
            number of filled pixels
        nclear : int
            number of clear (water or soil) pixels
        ntot : int
            total number of pixels 
        freq_clear : float 
            frequency of clear pixel among nclear-nstrip pixels
        sat : str
            satellite mission/platform id
        tile : str
            tile id
        
    
    References
    ----------
    Victor PENOT
    '''
    
    landsats=df['sat'].unique()
    
    tiles=df['tile'].unique()
    
    results=pd.DataFrame()
    
    for landsat in landsats :
        
        for tile in tiles :
            
            subdf=df.loc[(df['sat']==landsat) & (df['tile']==tile)]
            
            nstrip=len(subdf.loc[subdf[reflectance_quality_flag]==fill_no_data[landsat]])
            
            nclear=len(subdf.loc[subdf[reflectance_quality_flag].isin([clear_soil_pixel[landsat],clear_water_pixel[landsat]])])
            
            ntot=len(subdf)
            
            if ntot-nstrip>0 :
                freq=(nclear)/(ntot-nstrip)
            else :
                freq=0
            
            results=pd.concat([results,
                              pd.DataFrame({'nstrip':[nstrip],'nclear':[nclear],'ntot':[ntot],'freq_clear':[freq],
                                            'sat':[landsat],'tile':[str(tile)]})],ignore_index=True)
    
    return results

def freq_pixel_by_date(df:pd.DataFrame,
                       clear_soil_pixel:dict=clear_soil_pixel,
                       clear_water_pixel:dict=clear_water_pixel,
                       fill_no_data:dict=fill_no_data,
                       reflectance_quality_flag:str='qa_pixel'):
    """Parallelized version of freq_pixel 
    
    """
    return df.groupby('date').apply(freq_pixel,reflectance_quality_flag=reflectance_quality_flag)
"""
custom_mean = dd.Aggregation(
    name='custom_mean',
    chunk=lambda s: s.mean(),
    agg=lambda s0: s0.mean()
)  
"""
######################################### TEMP OF AIR AND MEAN BY DATES ###########################################
def get_tair(tair_disagg:pd.DataFrame,
             time_rsdata:pd.DataFrame,#=time_rsdata,
             col:str='ta_d',
             agg_col:str='date',
             start_hour :int = 10,
             end_hour : int = 11):
    
    ''' compute a pondered mean product value for each pixel and date-hour-minute when 
    product value are given at round hours between min and max hour.

    Parameters
    ----------
    tair_disagg : pd.DataFrame : pd.DataFrame
        dataframe at a given date containing product ("col") value per pixel at different hour. 
        Attributes must be date (format "YYYY-MM-DD"), hour, minute, col, and ind (unique pixel id)
    
    time_rsdata : pd.DataFrame
        dataframe of satellite time overpass. Must contain hour, minute, date (format "YYYY-MM-DD") 
    
    col : str
        column name of product in tair_disagg
    
    agg_col : str
        key value between tair_disagg and time_rs_data to be merged 
    
    start_hour : float
        minimum hour to 1/ filter satellite overpass and 2/ evaluate mean of product
    
    end_hour : float
        maximum hour to 1/ filter satellite overpass and 2/ evaluate mean of product
    
    Returns
    -------
    pd.DataFrame : 
        ind : int
            unique pixel id 
        col : float 
            pondered value of product 
        
    
    References
    ----------
    Victor PENOT
    '''
    
    date=tair_disagg[agg_col].unique()[0]

    ts_data=time_rsdata.loc[(time_rsdata['date']==date) & (time_rsdata['hour']>=start_hour) & (time_rsdata['hour']<=end_hour)]
    
    hour=ts_data['hour'].mean()

    minute=ts_data['minute'].mean()
    
    hour_min=tair_disagg.loc[tair_disagg['hour']<=hour]['hour'].max()
    
    hour_max=tair_disagg.loc[tair_disagg['hour']>hour]['hour'].min()
    
    tair_inter=tair_disagg.loc[tair_disagg['hour'].isin([hour_min,hour_max])]
    
    tair_inter=tair_inter.sort_values(by='hour', ascending=True)
    
    before=tair_inter.drop_duplicates(subset=['ind'],keep='first',ignore_index=True)
    before=before.rename(columns={col:'ta_d_b'})
    
    after=tair_inter.drop_duplicates(subset=['ind'],keep='last',ignore_index=True)
    after=after.rename(columns={col:'ta_d_a'})
    
    tair=before[['ind','ta_d_b']].merge(after[['ind','ta_d_a']],on='ind')
    
    tair[col]=((60-minute)*tair['ta_d_b']+minute*tair['ta_d_a'])/60
    
    return tair[['ind',col]]

def get_tair_by_date(tair_disagg:pd.DataFrame,time_rsdata:pd.DataFrame,#,=time_rsdata,
                     col:str='ta_d',agg_col:str='date',start_hour:float=10,end_hour:float=11):
    """Parallelized version of get_tair
    """
    return tair_disagg.groupby(agg_col).apply(get_tair,time_rsdata=time_rsdata,
                                              col=col,agg_col=agg_col,start_hour=start_hour,end_hour=end_hour).reset_index()[['ind',agg_col,col]]


def mean_tair(tair_disagg:pd.DataFrame,time_rsdata:pd.DataFrame,#=time_rsdata,
                     col:str='ta_d',agg_col:str='date',start_hour:float=10,end_hour:float=11):
    """ Mean of get_tair result
    """
    return get_tair(tair_disagg,time_rsdata=time_rsdata,col=col,agg_col=agg_col,start_hour=start_hour,end_hour=end_hour)[[col]].mean()
   

def mean_tair_by_date(tair_disagg:pd.DataFrame,time_rsdata:pd.DataFrame,#=time_rsdata,
                     col:str='ta_d',agg_col:str='date',start_hour:float=10,end_hour:float=11):
    """ Parallelized version of mean_tair
    """
    return tair_disagg.groupby(agg_col).apply(mean_tair,time_rsdata=time_rsdata,col=col,agg_col=agg_col,start_hour=start_hour,end_hour=end_hour).reset_index()[[agg_col,col]]

####################################### MINIMUM OF ONE ATTRUBIUTE AMONG CLEAR PIXELS
def min_col(df:pd.DataFrame,col:str,comp_val:float=0,
            clear_soil_pixel:dict=clear_soil_pixel,
            clear_water_pixel:dict=clear_water_pixel,
            reflectance_quality_flag:str='qa_pixel'):
    ''' Minimum value of a rs attribute among all clear pixels (water or soil)

    Parameters
    ----------
    df : pd.DataFrame
        dataframe at a given date-hour containing rsdata per pixel, particularly : 
            col, 
            reflectance_quality_flag, 
            sat (satellite) 
            tile 
    
    col : str 
        attribute name in df where evaluate minimum value
    
    comp_val : float 
        comparison value with min value  of col. Return max(com_val, min(col))
        
    clear_soil_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear soil pixels for each satellite type (key)
    
    clear_soil_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear water pixels for each satellite type (key)
        
    reflectance_quality_flag : str
        column name in df of reflectance quality flag 
    
    Returns
    -------
    pd.DataFrame : 
        'min_'+col : float
            max(com_val, min(col))
        sat : str
            satellite mission/platform id
        tile : str
            tile id 
        
    
    References
    ----------
    Victor PENOT
    '''
    landsats=df['sat'].unique()
    tiles=df['tile'].unique()
    results=pd.DataFrame()
    
    for landsat in landsats :
        
        for tile in tiles :
            
            subdf=df.loc[(df['sat']==landsat) & (df['tile']==tile)]
            min_tir=max(subdf.loc[subdf['qa_pixel'].isin([clear_soil_pixel[landsat],clear_water_pixel[landsat]])][[col]].min()[0],comp_val)
            
            if pd.isna(min_tir)==False :
                results=pd.concat([results,
                                    pd.DataFrame({'min_'+col.lower() :[min_tir],
                                                    'sat':[landsat],'tile':[str(tile)]})],ignore_index=True)
            else :
                return None
    return results

def min_col_by_date(df:pd.DataFrame,col:str,comp_val:float=0,
                    clear_soil_pixel:dict=clear_soil_pixel,
                    clear_water_pixel:dict=clear_water_pixel,
                    reflectance_quality_flag:str='qa_pixel'):
    """ Parallelized version of min_col
    """
    return df.groupby('date').apply(min_col,col=col,comp_val=comp_val,reflectance_quality_flag=reflectance_quality_flag)

