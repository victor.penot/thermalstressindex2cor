from packages import * 

def create_sta_grid(path2sta:str,x:str='lon', y:str='lat', z:str='height_sta', 
                    unique_id:str="number_sta", 
                    sta_epsg:int=4326, sta_prefix:str="SE"):
    ''' Create geopandas GeoDataFrame of station locations from ground station meteo files. 
    Explicitely from meteonet datasets : https://meteonet.umr-cnrm.fr/

    Parameters
    ----------
    path2sta : str 
        path to folder containing ground meteo data 
    x : str
        column name of longitude/x coordinate in ground meteo files 
    y : str
        column name of latitude/y coordinate in ground meteo files 
    z : str
        column name of elevation/z in ground meteo files
    unique_id : str 
        column name of unique identifiant of each ground station in ground meteo files
    sta_epsg : int
        epsg number of x,y crs
    prefix : str 
        common prefix of ground meteo files in .csv format 
    
    
    Returns
    -------
    gpd.GeoDataFrame of ground stations with z and unique id attributes, in sta_epsg crs.
    
    
    References
    ----------
    Victor PENOT
    '''
    # open ground meteo files 
    meteo_points=dd.read_csv(os.path.join(path2sta,sta_prefix+"*.csv"))
    
    # get unique id, x, y, and z
    number_sta=meteo_points.groupby([unique_id,x,y,z]).count()
    
    # remove duplicate
    number_sta=number_sta.reset_index().drop_duplicates(subset=[unique_id]).compute()
    
    # to GeoDataFrame 
    number_sta = gpd.GeoDataFrame(number_sta[[unique_id,z]], 
                                        geometry=gpd.points_from_xy(number_sta[x], number_sta[y]),
                                       crs="epsg:"+str(sta_epsg))
    return number_sta

def select_sta_grid(path2sta:str,bbox:gpd.GeoDataFrame,
                    buffer_distance :float=3*sqrt(2)*8000):
    
    ''' Select ground station locations in and around (+ buffer) the bounding box of the study area 

    Parameters
    ----------
    path2sta : str 
        path to folder containing station location file named sta_gridpoints.shp
    bbox : gpd.GeoDataFrame
        GeoDataFrame of the bounding box of the study area 
        
    buffer_distance : float
        buffer distance [m] or unit of bbox crs to select ground station location
    
    Returns
    -------
    gpd.GeoDataframe of ground staions with z and unique id attributes in bbox crs
    
    
    References
    ----------
    Victor PENOT
    '''
    
    # open stations location file
    if os.path.exists(os.path.join(path2sta,"sta_gridpoints.shp")):
        sta_grid=gpd.read_file(os.path.join(path2sta,"sta_gridpoints.shp"))
    else :
        "File does not exist"
        return None
    
    # get bbox crs 
    scrs=bbox.crs
    
    # set stations crs to bbox crs 
    if sta_grid.crs != scrs :
        sta_grid.to_crs(scrs,inplace=True)
        
    # select meteo stations 
    sta_select = gpd.sjoin(sta_grid, 
                           gpd.GeoDataFrame(geometry=bbox.buffer(buffer_distance),crs=scrs),
                           predicate='within')
    return sta_select

def create_safran_grid(path2safran:str,prefix_product:str="ForcT",x_safran:str='x',y_safran:str='y',safran_crs:int=27572):
    
    ''' Create geopandas GeoDataFrame of reanalysis grid centroids from meteo reanalysis files   

    Parameters
    ----------
    path2safran : str 
        path to folder containing reanalysis files "*.nc"
    prefix_product : str 
        common prefix of reanalysis files containing all location 
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files     
    safran_crs : int
        epsg number of x,y crs
    
    
    Returns
    -------
    gpd.GeoDataframe of reanalysis grid points in safran_crs crs
    
    
    References
    ----------
    Victor PENOT
    '''
    
    ds = xr.open_mfdataset(os.path.join(path2safran,prefix_product+"*.nc"),combine='by_coords',parallel=True)
    
    x=ds.product[x].load().to_numpy()
    
    y=ds.product[y].load().to_numpy()
    
    xy=list(itertools.product(x,y))
    
    df=pd.DataFrame({x_safran:[i[0] for i in xy],y_safran:[i[1] for i in xy]})
    
    safran_grid=gpd.GeoDataFrame(df,geometry=gpd.points_from_xy(df[x_safran], df[y_safran]),crs='epsg:'+str(safran_crs))
    
    return safran_grid

def select_safran_grid(path2safran:str,
                       bbox:gpd.GeoDataFrame,
                       x_safran:str='x',y_safran:str='y',
                       buffer_distance:float=1.5*sqrt(2)*8000) :
    
    ''' Select reanalysis centroids locations in and around (+ buffer) the bounding box of the study area 

    Parameters
    ----------
    path2safran : str 
        path to folder containing reanalysis centroids location file named safran_gridpoints.shp
    bbox : gpd.GeoDataFrame
        GeoDataFrame of the bounding box of the study area 
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files  
    buffer_distance : float
        buffer distance in units of bbox crs to select reanalysis centroids location
    
    Returns
    -------
    gpd.GeoDataframe of reanalysis centroid locations in safran_crs 
    x_select : x coordinates of reanalysis gridpoints in safran_crs 
    y_select : y coordinates of reanalysis gridpoints in safran_crs
    
    
    References
    ----------
    Victor PENOT
    '''
    
    safran_grid=gpd.read_file(os.path.join(path2safran,"safran_gridpoints.shp"))
    
    safran_crs=safran_grid.crs
    
    scrs=bbox.crs
        
    safran_select = gpd.sjoin(safran_grid.to_crs(scrs), 
                           gpd.GeoDataFrame(geometry=bbox.buffer(buffer_distance),crs=scrs),
                           predicate='within')

    safran_select.to_crs(safran_crs,inplace=True)
        
    x_select=xr.DataArray(safran_select[x_safran],dims=['location']) # location
    
    y_select=xr.DataArray(safran_select[y_safran],dims=['location']) # location
    
    return safran_select,x_select,y_select

def get_points_topo(safran_select:gpd.GeoDataFrame,
                    path2dem:str,
                    parameters:list=['elevation'],
                    parameters_index:dict={'elevation':0,'slope':1,'aspect':2}):
    
    ''' get topographic indexes/parameters at points location from dem 

    Parameters
    ----------
    safran_select : gpd.GeoDataFrame
        GeoDataFrame of points 
    path2dem:str
        path to dem file (possibly multilayer / see below )
    parameters:list 
        list of names of index to extract (correspinding position in dem is describe as follows)
    parameters_index
        dictionnary linking the name of the index and its layer number in the dem
       
    Returns
    -------
    gpd.GeoDataframe same as input with as many more columns corresponding to "parameters" list
    
    
    References
    ----------
    Victor PENOT
    '''
    
    with rasterio.open(path2dem,'r') as raster :
        
        extract_grid=safran_select.to_crs(raster.crs)
        
        coord=[[x,y] for x,y in zip(extract_grid['geometry'].x,extract_grid['geometry'].y)]
        
        data=np.array([sample for sample in raster.sample(coord)])
    
    for param in parameters :
        safran_select[param]=data[:,parameters_index[param]]
        
    return safran_select

def get_points_location(points_path:str,point_file:str=None,
                        safran_crs:int=27572,path2dem:str=None,
                        x_safran:str='x',y_safran:str='y',
                        parameters:list=['elevation'], parameters_index:dict={'elevation':0,'slope':1,'aspect':2},
                        longlat:bool=False,init_coord:bool=False):
    
    ''' open point files and create supplementary spatial attributes 

    Parameters
    ----------
    points_path:str 
        path to point file if .shp or to .gpkg
    point_file:str
        None if points_path is .shp of name of layer if points_path is gpkg
    safran_crs : int 
        epsg number reanalysis data
    path2dem:str
        path to dem file (see get_points_topo)
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files 
    parameters:list 
        if dem is not None, list of index to extract from dem 
    parameters_index : dict
        if dem is not None, dictionnary linking the name of the index and its layer number in the dem
    longlat : bool
        if True add longitude and latitude (lon,lat) attributes in out dataframe
    init_coord:bool
        if True add initial coordinates (x_i,y_i) in attributes of out dataframe

    Returns
    -------
    pd.Dataframe : with as many more columns corresponding to "parameters" list, lonlat ,init_coord, 
        and x,y coordinates in safran_crs
    xf, yf : xr.DataArray corresponding to coordinates in safran_crs to interpolate 
    
    
    References
    ----------
    Victor PENOT
    '''
    
    # lecture du fichier de points 
    if point_file !=None :
        points = gpd.read_file(points_path, layer=point_file)
    else :
        points = gpd.read_file(points_path)
        
    # crs origine 
    scrs=points.crs
    
    coord_name=['x','y']
    
    if init_coord==True :
        points["x_i"]=points.geometry.x
        points["y_i"]=points.geometry.y
        coord_name+=['x_i','y_i']
        
    
    # get altitude of points from mnt de landsat 
    if path2dem !=None:
        points=get_points_topo(points,path2dem,parameters=parameters,parameters_index=parameters_index)
        
    # change crs to safran one
    points.to_crs(safran_crs,inplace=True)
    
    # save safran crs coordinates
    points[x_safran]=points.geometry.x
    points[y_safran]=points.geometry.y
    
    # coordonnéees des points a interpoler dans  le crs safran
    xf=xr.DataArray(points.geometry.x, dims=['points'])
    yf=xr.DataArray(points.geometry.y, dims=['points'])
    
    if longlat==True :
        if x_safran!='lon' and y_safran!='lat' :
            # change crs to safran one
            points.to_crs('epsg:4326',inplace=True)
            # save original coordinates 
            points["lon"]=points.geometry.x
            points["lat"]=points.geometry.y
            coord_name+=['lon','lat']
    
    return points[['ind']+parameters+coord_name],xf,yf

def get_points_bbox(points_path:str,point_file:str=None):
    
    ''' get bounding box of study area  

    Parameters
    ----------
    safran_select : gpd.GeoDataFrame
        GeoDataFrame of points 
    path2dem:str
        path to dem file (possibly multilayer / see below )
    parameters:list 
        list of names of index to extract (correspinding position in dem is describe as follows)
    parameters_index
        dictionnary linking the name of the index and its layer number in the dem
       
    Returns
    -------
    gpd.GeoDataframe :  same as input with as many more columns corresponding to "parameters" list
    
    
    References
    ----------
    Victor PENOT
    '''
    if point_file !=None :
        with fiona.open(points_path, "r", layer=point_file) as src:
            bbox=box(*src.bounds)
            scrs=pyproj.CRS(src.crs['init'])
            
    else :
        with fiona.open(points_path, "r") as src:
            bbox=box(*src.bounds)
            scrs=pyproj.CRS(src.crs['init'])
    return gpd.GeoDataFrame(geometry=gpd.GeoSeries(bbox,crs=scrs))

