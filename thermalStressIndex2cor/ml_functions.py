from packages import * 
from initialization import *


class data_generator(Sequence):
    def __init__(self, x_set, y_set, batch_size):
        self.x, self.y = x_set, y_set
        self.batch_size = batch_size

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]
        return batch_x, batch_y

def preprocess_datatrain(datatrain:pd.DataFrame, 
                        predictors:list=['ta','elevation','dta','ta_mean_day','ta_sd_day'],
                        yobj:str='ta_ref'):
    
    ''' center/normalize a dataset  

    Parameters
    ----------
    datatrain : pd.DataFrame
        dataframe containing predictors and yobj attributes to normalize, n lines
    predictors : list 
        list of size p of attributes to predict (type str)
    yobj : str
        objective atttribute
    
    
    Returns
    -------
    X : np.array 
        (column = normalized predictors) of size nxp
    y : np.array 
        size nx1 corresponding to the objective attribute 
    

    
    References
    ----------
    Victor PENOT
    '''
    
    ## dataset selection / normalization
    X = datatrain[predictors].copy()
    
    # apply normalization techniques
    for column in X.columns:
        X[column] = (X[column] -X[column].mean()) / X[column].std() 
    
    X=X.to_numpy()
    
    y = datatrain[yobj].to_numpy()
    
    return X,y

def train_model_nn(datatrain:pd.DataFrame,name_model:str,
                   predictors:list=['ta','elevation','dta','ta_mean_day','ta_sd_day'],yobj:str='ta_ref',
                   batch_size:int=4096,epochs:int=200, learning_rate:float=0.001,ncpu:int=10) :
    
    ''' train a Neural Network

    Parameters
    ----------
    datatrain : pd.DataFrame
        dataframe containing predictors and yobj attributes 
    name_model : str
        name of the model 
    predictors : list 
        list of size p of attributes to predict (type str)
    yobj : str
        objective atttribute
    batch_size : int
        NN parameters
    epochs : int
        NN parameters
    learning_rate : float 
        NN parameters 
    ncpu : int
    	number of cpu

    Returns
    -------
    history : tf.keras.callbacks.History
    model : tf.keras.Model
    
    References
    ----------
    Victor PENOT
    '''
    
    
    
    # preprocess datatrain dataset
    X,y=preprocess_datatrain(datatrain,predictors=predictors,yobj=yobj)
    
    # split train/test
    Xtrain, Xval, ytrain, yval = train_test_split(X, y,test_size=0.20)
    
    # creation of generators
    training = data_generator(Xtrain, ytrain, batch_size)
    validation = data_generator(Xval, yval, batch_size)
    training_count=Xtrain.shape[0]
    validation_count=Xval.shape[0]
    depth=Xtrain.shape[1]

    # optimizers
    opt=keras.optimizers.Adam(learning_rate=learning_rate)
    
    # Calbacks 
    callbacks = [
        keras.callbacks.ModelCheckpoint(name_model,
                                        save_best_only=True,
                                       monitor='val_loss', mode='min'),
        keras.callbacks.TensorBoard(log_dir="./logs",histogram_freq=1), 
    ]
    ###################################################################
    #                 Neural network implementation
    ####################################################################
    inputs = keras.Input(shape=(depth))
    x = layers.Dense(1024,activation='relu')(inputs)
    x = layers.Dense(512,activation='relu')(x)
    x0 = layers.Dense(256,activation='relu')(x)
    x1 = layers.Dense(128,activation='relu')(x0)
    x2 = layers.Dense(64,activation='relu')(x1)
    x3 = layers.Dense(32,activation='relu')(x2)
    x4 = layers.Dense(16,activation='relu')(x3)
    outputs=layers.Dense(1)(x4)
    model = keras.Model(inputs, outputs)
    #print(model.summary())

    # model
    model.compile(optimizer=opt, loss="mse", metrics=["mae"])
    # Training 
    history = model.fit(training,
                        batch_size=batch_size,
                        steps_per_epoch=int(training_count/batch_size),
                        epochs=epochs,
                        validation_data=validation,
                        validation_steps=int(validation_count/batch_size),
                        callbacks=callbacks,
                        use_multiprocessing=True,
                        workers=ncpu,
                        )
    return history,model

