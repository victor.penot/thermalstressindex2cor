from packages import *

def get_min_solar_angles_np(lon:float,lat:float,elevation:float,hour:int=10,minute:int=30):
    
    ''' get minimum solar zenith angle at a given point location and a given hour  

    Parameters
    ----------
    lon: float 
        longitude [deg]
    lat : float 
        latitude [deg]
    elevation : float
        elevation [m]
    hour : int /!\ >=10
        hour
    minute : int
        minute
       
    Returns
    -------
    zenith : float
        solar zenith angle [deg]
    
    
    References
    ----------
    Victor PENOT
    '''
    if minute <10 :
        minute='0'+str(minute)
    else :
        minute=str(minute)
    times = pd.date_range('2022-01-01 '+str(hour)+':'+minute+':00', 
                          '2022-12-31 '+str(hour)+':'+minute+':00', 
                           freq='D')
    min_zen= pvlib.solarposition.get_solarposition(times,lat, lon,elevation)['zenith'].min()
    
    #rint(min_zen)
    
    return min_zen


def get_solar_angles(points:pd.DataFrame,date:str,hour:int,minute:int) :
    
    ''' get solar zenith angle at given point locations and a given hour  

    Parameters
    ----------
    points : pd.DataFrame
        Dataframe containingat leat ind (unique id of points), lat, lon, elevation
    date : str
        date YYYY-MM-DD
        
    hour : int /!\ >=10
        hour
    minute : int
        minute
       
    Returns
    -------
    zenith : float
        solar zenith angle [deg]
    
    
    References
    ----------
    Victor PENOT
    '''
    
    subpoints=points[['lon','lat','elevation','ind']].copy()
        
    if minute<10 :
        dtime=pd.to_datetime(date+"T"+str(hour)+":0"+str(minute)+":00")
    else :
        dtime=pd.to_datetime(date+"T"+str(hour)+":"+str(minute)+":00")
            
    subpoints['time']=dtime
        
    subpoints['zenith']=pvlib.solarposition.get_solarposition(subpoints['time'],
                                                                      subpoints['lat'],
                                                                      subpoints['lon'], 
                                                                      subpoints['elevation']).reset_index()['zenith']
    subpoints['date']=date
    subpoints['hour']=hour
    subpoints['minute']=minute
        
    return subpoints[['zenith','ind','date','hour','minute']]


def get_cumul_rain(safran_rain:xr.Dataset,
                   x_select:xr.DataArray,
                   y_select:xr.DataArray,
                   name_product:str,
                   time_safran:str,
                   x_safran:str,
                   y_safran:str,
                   rain_timelag:int,
                   start_hour:int,
                   fact_rain:float,
                   time_step_per_day:int=24) :
    
    ''' Evaluate statistical cumulative values of reanalysis product over 
    rain_timelag days at a given hour of the day

    Parameters
    ----------
    safran_rain:xr.Dataset
        xarray dataset from .nc files containing raw data of product 
    x_select : xr.DataArray
        x coordinates of selected reanalysis gridpoints in safran_crs 
    y_select : xr.DataArray
        y coordinates of selected reanalysis gridpoints in safran_crs
    name_product : str
        name of the product in .nc files  
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files
    rain_timelag :int
        length of cumulativ period in number of days 
    start_hour:int
        last entire hour before satelite overpass time 
    fact_rain : float 
        multiplicative factor to set units of results in mm.
    time_step_per_day : int
        number of time step of reanalysis data in one day 

    Returns
    -------
    rain_cumsum : pd.DataFrame 
        time_safran : date / same format as time_safran
        name_product : cumulative value of product over rain_timelag*time_step_per_day before start_hour

    References
    ----------
    Victor PENOT
    '''

    # rolling sum of rain 
    rain_cumsum=safran_rain.sel({x_safran:x_select,
                                 y_safran:y_select})[name_product].rolling({time_safran:rain_timelag*time_step_per_day}, 
                                                                             center=False).sum()

    rain_cumsum=rain_cumsum.isel({time_safran:rain_cumsum[time_safran].dt.hour.isin([start_hour])}).mean(dim='location').load()

    # to dataframe
    rain_cumsum=rain_cumsum.to_dataframe().reset_index()
    
    # per second to per hour
    rain_cumsum[name_product]=rain_cumsum[name_product]*fact_rain
    
    return rain_cumsum

def get_rg_samani(lon:float,lat:float,alti:float, aspect:float, 
                  slope:float, date:datetime.datetime,hour:int=10,minute:int=30, 
                  pluslon:float=0.0,timezone:float=0.0):
    ''' RG / incident incoming short wave radiation  by Samani method 

    Parameters
    ----------
    lon : float
        longitude [deg]
        
    lat : float
        longitude [deg]
    
    alti : float
        altitude / elevation [m]
    
    aspect : float
        aspect relative to north (0 : north / 180 : south ) [deg]
    
    slope : float
        slope [deg]
    
    date : datetime.datetime
         date / at least YYYY-MM-DD
    
    hour : int
        hour
    
    minute : int
        minute
        
    pluslon : float
        plus longitude 
    
    timezone : float
        additionnal time as regard greenwich 
    

    
    Returns
    -------
    Rg/rsi : float 
        incident incoming short wave radiation [W/m2]
        
    
    References
    ----------
    Samani, Zohrab & Bawazir, A. & Bleiweiss, Max & Skaggs, Rhonda & Tran, Vien. (2007). Estimating Daily Net Radiation over Vegetation Canopy through Remote Sensing and Climatic Data. Journal of Irrigation and Drainage Engineering-asce - J IRRIG DRAIN ENG-ASCE. 133. 10.1061/(ASCE)0733-9437(2007)133:4(291). 
    '''
    # elevation [m]
    
    alti=alti
    
    # aspect [rad] south facing
    aspect=((aspect+180.0)%360)*math.pi/180
    
    # slope [rad]
    slope = (slope+0.0)*math.pi/180
    
    # hour
    hour=hour+minute/60
    
    # longitude [deg]
    lon = lon + pluslon
    
    # solar declination [rad]
    j=int(date.strftime('%j'))
    delta = (23.45 * np.sin(math.pi/180*(360.0/365.0*(284.0 +j))))*math.pi/180
    
    # et = Equation of Time (correction (h) which accounts for perturbation in earth’s rotation rate) [rad]
    b=math.pi/180*(360/365*(j-81))
    et = 9.87*np.sin(2.0*b) - 7.53 *np.cos(b) - 1.5*np.sin(b)
    
    # AST = Apparent Solar Time [h] 
    ast = hour  - 4.0*(15.0*timezone-lon)/60.0 + et/60.0+ (0.22)
    
    # omega = Solar Time Angle [rad]
    solar_time = math.pi/180.0*((ast - 12.0) * 15.0)
    
    # latitude en radian [rad]
    lat_rad=lat*math.pi/180.0
    
    # theta = Solar Incence Angle [rad]
    cos_theta = np.sin(lat_rad)*np.sin(delta)*np.cos(slope)
    cos_theta =cos_theta- np.cos(lat_rad)*np.sin(delta)*np.sin(slope)*np.cos(aspect)
    cos_theta =cos_theta+np.cos(lat_rad)*np.cos(delta)*np.cos(solar_time)*np.cos(slope)
    cos_theta =cos_theta+np.sin(lat_rad)*np.cos(delta)*np.cos(solar_time)*np.sin(slope)*np.cos(aspect)
    cos_theta =cos_theta+np.cos(delta)*np.sin(solar_time)*np.sin(slope)*np.sin(aspect)

    
    # Solar Constant [W/m2]
    gs = 1.367 
    tsw = 0.75 + 2.0 * 10**(-5)*alti # Athmospheric transmissivity from elevation
    dr = 1 + 0.033 * np.cos(2.0*math.pi*j/365) # Inverse relative earth-sun distance
    
    # Incident incoming short wave radiation [W/m2]
    rsi=gs*cos_theta*dr*tsw
    rsi = np.where(rsi<0,0,rsi)*1000
    
    return(rsi)
  
def interpol_param_safran(safran_data:xr.Dataset,
                          xf:xr.DataArray,yf:xr.DataArray,
                          dtime:np.datetime64,
                          name_out:str,
                          name_product:str='product',
                          x_safran:str='x',y_safran:str='y',time_safran:str='time'):
    ''' spatial linear interpolation of a product at a given time 

    Parameters
    ----------
    safran_data : xr.Dataset
        DataSet continaing product and x,y (long/x, lat/y in safran_crs), and time coordinates
        
    xf: xr.DataArray 
        corresponding to coordinates longitude / x in safran_crs to interpolate 
    
    yf: xr.DataArray 
        corresponding to coordinates latitude / y in safran_crs to interpolate
    
    dtime:np.datetime64 
        time to interpolate 
    
    name_out : str
        name of the product in out DataFrame
        
    name_product : str
        name of the product in safran_data
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files 
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files
    
    Returns
    -------
    pd.DataFrame : 
        x : float long/x coordinates in safran_crs
        y : float lat/y coordinates in safran_crs
        name_out : linearly interpollated product at x, y and dtime
        
    
    References
    ----------
    Victor PENOT
    '''
    
    df=safran_data[name_product].interp({time_safran:dtime},method='linear').interp({x_safran:xf,y_safran:yf},method="linear").load().rename(name_out)
    
    df=df.to_dataframe()[[x_safran,y_safran,name_out]]
    
    return df


def qair2rh(ta :float,q:float,press:float=1013.25) : # temp en degre K / qair / press en hPa
    
    ''' convert specific humidity to relative humidity 

    Parameters
    ----------
    ta : float 
        air temperature [K]
        
    q : float 
        specific humidity [g/kg]
        
    press : float 
        atmospheric pressure [hPa]
    
    Returns
    -------
    rh : float 
        relative humidity [0,1]
        
    
    References
    ----------
    Victor PENOT
    '''
    es = 6.112 * np.exp((17.67 * (ta-273.15))/((ta-273.15) + 243.5))
    
    e = q * press / (0.378 * q + 0.622)
    
    rh = e / es
    
    rh=np.where(rh > 1,1,rh)
    
    rh=np.where(rh < 0, 0,rh)
    
    return(rh)

def get_air_radiation(ta:float,ha:float,sigma:float=5.67*10**(-8)):
    
    ''' compute long wave incoming air radiation

    Parameters
    ----------
    ta : float 
        air temperature [K]
        
    ha : float 
        relative humidity [-]
        
    sigma : float 
        stephan-boltzmann constant [J.s^-1.m^-2.K^-4]
        
    Returns
    -------
    ra : float
        atmospheric longwave incoming radiation [W.m^-2]
        
    References
    ----------
    Victor PENOT / Yohan MALBETEAU
    '''
    
    # actual pressure
    ea=ha*611*np.exp(17.27*(ta-273.15)/(ta-35.9)) #  Pa
    
    # emissivity of air 
    emia = 0.553*(ea/100)**(1/7) 
    
    # atmospheric radiation
    ra=emia*sigma*ta**4
    
    return ra 
