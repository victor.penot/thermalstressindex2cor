from packages import * 
from gis_functions import *
from ml_functions import *
from parameters import *


"""
from math import sqrt 
import geopandas as gpd
import pandas as pd
import xarray as xr
import numpy as np
import dask.dataframe as dd
import os.path
"""


def get_safran_data(dtime:pd.Timestamp,safran_select:gpd.GeoDataFrame,safran_data:xr.Dataset,
                    name_product:str='product',
                    x_safran:str="x",y_safran:str="y",time_safran:str='time',
                    name_out:str='ta') :
    
    ''' Evaluate statistical values of reanalysis product for a given time and locations

    Parameters
    ----------
    dtime:pd.Timestamp 
        when statistical indexes are calculated 
    
    safran_select : gpd.GeoDataFrame
        GeoDataFrame of the selected reanalysis points grid (see "select_safran_grid")
    
    safran_data:xr.Dataset
        xarray dataset from .nc files containing raw data of product / location are already selected
    name_product : str
        name of the product in .nc files 
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files 
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files
    name_out:str
        name used to identify product statistics in out dataframe 
    Returns
    -------
    pd.DataFrame :
        x,y : coordinates of reanalysis point grid / locations
        name_out,: value of product at dtime at each location
        name_out+'_sd_day' : standard deviation of product over the dtime day at each location
        name_out+'_mean_day' : mean of product over the dtime day at each location
        elevation : elevation [m] at each location
        dta : lapse rate over the whole locations 
        date : dtime 
    
    
    
    References
    ----------
    Victor PENOT
    '''
    
    # data per day 
    day=pd.to_datetime(pd.to_datetime(dtime).strftime("%Y%m%d"))
    
    day_range=pd.date_range(start=day, end=day+pd.Timedelta(23, "h"),freq='H')

    safran_data=safran_data.sel({time_safran:day_range},method='nearest')
    
    safran_data=safran_data.dropna(dim="location", how="any")
    
    location_ok=safran_data.location
    
    #print(np.array(location_ok).shape)
    
    tair_mean_day=safran_data[name_product].mean(dim=[time_safran]).rename(name_out+"_mean_day")
    
    tair_sd_day=safran_data[name_product].std(dim=[time_safran]).rename(name_out+"_sd_day")
    
    # data per hour 
    safran_data=safran_data.sel({time_safran:dtime}).sel(location=location_ok,method='nearest')#x=x_select, y=y_select))
    
    tair=safran_data[name_product].rename(name_out)   
    
    # datatrain creation 
    datatrain=xr.merge([tair,tair_mean_day,tair_sd_day]).to_dataframe()[[x_safran,y_safran,name_out,name_out+'_sd_day',name_out+'_mean_day']]
    
    # dta lapse rate 
    datatrain=datatrain.merge(safran_select[[x_safran,y_safran,'elevation']], how='left', on=[x_safran,y_safran])
    
    regressor = LinearRegression()
    
    regressor.fit(datatrain['elevation'].to_numpy().reshape(-1, 1) , 
                    datatrain[name_out].to_numpy().reshape(-1, 1))
    
    # lapse rate
    dta=regressor.coef_[0][0]
    
    datatrain['dta']=min(dta,0)
    
    # date / hour
    datatrain['date']=dtime
    
    return(datatrain[[x_safran,y_safran,name_out,name_out+'_sd_day',name_out+'_mean_day','elevation','dta' ,'date']])

def create_datatrain(path2sta:str,
                     path2safran:str,
                     bbox:gpd.GeoDataFrame,
                     
                     # satellite overpass time interval
                     start_hour:int=10,end_hour:int=11,
                     
                     # reanalysis inputs
                     safran_res:float=8000,
                     prefix_product : str="ForcT",
                     name_product:str='product',
                     x_safran:str='x',
                     y_safran:str='y',
                     time_safran:str='time',
                     name_out:str ='ta',
                     
                     # ground station inputs 
                     prefix_sta:str="sta",
                     unique_id_sta:str="number_sta",
                     sta_name_product:str="t",
                     time_sta:str='date',
                     z_sta:str="height_sta",
                     
                     # dem 
                     path2dem:str=None):
    
    ''' Creation of training dataset  

    Parameters
    ----------
    path2sta : str 
        path to folder containing station location file named sta_gridpoints.shp and .csv file (raw data)
        
    path2safran : str 
        path to folder containing reanalysis centroids location file named safran_gridpoints.shp and .nc files (raw data)
    
    bbox : gpd.GeoDataFrame
        GeoDataFrame of the bounding box of the study area 
    
    start_hour:int
        min hour to evaluate training data 
    
    end_hour:int
        max hour to evalute training data 
        
    safran_res:float
        resolution of the reanalysis grid in safran_crs unit 
    
    prefix_product : str 
        common prefix of reanalysis files containing all location 
    
    name_product : str
        name of the product in .nc files 
        
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files    
    
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files
    
    name_out:str
        name used to identify product statistics in out dataframe 
    
    prefix_product_sta : str 
        common prefix of ground station meteo data files
        
    unique_id_sta : str 
        column name of unique identifiant of each ground station in sta_gridpoints.shp
    
    sta_name_product :str 
        colum name of product in ground station dataset
    
    time_sta:str
        colum name of date/time in ground station dataset format YYYY-MM-DD hh:mm:ss
    
    z_sta : str 
        column name of elevation [m] in ground station data
    
    path2dem:str
        path to dem file to get elevation of reanalysis and study area elevation 
    
    Returns
    -------
    pd.Dataframe of training data for each hour between start_hour and end_hour at reanalysis grid locations
        x,y : coordinates of reanalysis point grid / locations in safran crs 
        name_out,: value of product at dtime at each location
        name_out+'_sd_day' : standard deviation of product over the dtime day at each location
        name_out+'_mean_day' : mean of product over the dtime day at each location
        elevation : elevation [m] at each location
        dta : lapse rate over the whole locations 
        date : dtime 
        name_out+"ref" : objective product value 
    References
    ----------
        B. Sebbar et al., “Machine-Learning-Based Downscaling of Hourly ERA5-Land Air Temperature over Mountainous Regions”, Atmosphere, vol. 14, no. 4, p. 610, Mar. 2023, doi: 10.3390/atmos14040610.    '''
    ##################################### GROUND STATION DATA #####################################
    # select ground station
    sta_select=select_sta_grid(path2sta,bbox,buffer_distance =3*sqrt(2)*safran_res) 
    
    print('# Ground stations selected')
    print('---> Number of ground stations selected :',len(sta_select))
    
    # select ground meteo data 
    meteo_data=dd.read_csv(os.path.join(path2sta,prefix_sta+"*.csv"))
    
    meteo_data=meteo_data.loc[(meteo_data[unique_id_sta].isin(sta_select[unique_id_sta]))][[time_sta,unique_id_sta,sta_name_product]].compute()
    
    print('# Ground station meteorological data loaded')
    
    meteo_data[time_sta]=pd.to_datetime(meteo_data[time_sta])
    
    # hour selection 
    meteo_data=meteo_data.set_index(time_sta).between_time(str(start_hour)+":00",str(end_hour)+":00").reset_index()
    
    # get station height
    meteo_data=pd.merge(meteo_data,sta_select[[unique_id_sta,z_sta]],on=unique_id_sta)
    
    # year in trainning 
    year2train=meteo_data[time_sta].dt.year.unique()
    
    ##################################### REANALYSIS DATA #####################################
    print('# Loading reanalysis meteorological tair data ...')

    # selected point grid meteo 
    safran_select,x_select,y_select=select_safran_grid(path2safran,bbox,
                                                       x_safran=x_safran,y_safran=y_safran,
                                                       buffer_distance=1.5*sqrt(2)*safran_res)  
    
    # if no elevation in safran select 
    if path2dem !=None :
        safran_select=get_points_topo(safran_select,path2dem)   
    
    safran_select=safran_select.loc[(safran_select["elevation"]>=-100)].copy()
    
    # open safran data in parallel
    safran_data = xr.open_mfdataset(
        os.path.join(path2safran,prefix_product+"*.nc"), combine='by_coords',parallel=True)
    """
    if  not x_safran in safran_data.coords :
        print("Change name "+x_safran+" to x")
        safran_data=safran_data.rename({x_safran: 'x'})
    if not y_safran in safran_data.coords :
        print("Change name "+y_safran+" to y")
        safran_data=safran_data.rename({y_safran: 'y'})
    """
        
    # selection of locations
    safran_data=safran_data.sel({x_safran:x_select,y_safran:y_select})
    
    # selection of years 
    safran_data=safran_data.isel({time_safran:safran_data[time_safran].dt.year.isin(year2train)}).load()
    
    # print(safran_data.coords)
    
    print("Safran data loaded")
    
    #################################### BUILDING DATATRAIN ###################################
    
    # final (empty) dataframe with training data
    datatraintot=pd.DataFrame()
    
    # loop over date-hour in ground meteo data
    for dtime in meteo_data[time_sta].unique() :
        
        hour=pd.to_datetime(dtime).hour
        
        minute=pd.to_datetime(dtime).minute
        
        # full hour selection
        
        if minute==0 :
            
            # get ground data of dtime
            
            meteo_data_date=meteo_data.loc[meteo_data[time_sta]==dtime]
            
            Xtrain=meteo_data_date.loc[~meteo_data_date[sta_name_product].isna()][z_sta].to_numpy().reshape(-1, 1) 
            
            ytrain=meteo_data_date.loc[~meteo_data_date[sta_name_product].isna()][sta_name_product].to_numpy().reshape(-1, 1) 
            
            # if at least three points 
            
            if len(ytrain)>=3 :
                
                print(dtime)
                
                # linear regression with ground data
                regressor = LinearRegression()
                
                regressor.fit(Xtrain, ytrain)
                
                # lapse rate from ground data and intercept 
                dta_train=regressor.coef_[0][0]
                
                # predictors from safran data 
                
                datatrain=get_safran_data(dtime,safran_select,safran_data,
                                          name_product=name_product,
                                          x_safran=x_safran,y_safran=y_safran,
                                          time_safran=time_safran,
                                          name_out=name_out)
                
                # name_out+_ref : target name_out to train
                if dta_train < 0 :
                    datatrain[name_out+'_ref']=regressor.predict(datatrain['elevation'].to_numpy().reshape(-1, 1))
                else :
                    datatrain[name_out+'_ref']=datatrain[name_out]
                
                # lapse rate from ground data still negative
                # datatrain['dta_train']=min(dta_train,0)
                
                # concatenation with all dataset
                datatraintot=pd.concat([datatraintot, datatrain])
                
                del regressor,datatrain
                
    return datatraintot
    
def get_date(list_hour:list,listdate:list):
    ''' create list of pd.datetime object 

    Parameters
    ----------
    list_hour:list 
        liste of hour (type : int) when disaggregrate 
    list_date : list 
        list of date (type: str) ("YYYY-MM-DD") when disaggregrate 

    Returns
    -------
    list : list of date-hour (type : pd.datetime)

    
    References
    ----------
    Victor PENOT
    '''
    list_finale=[]
    for hour in list_hour :
        for date in listdate :
            year=int(date[0:4])
            month=int(date[5:7])
            day=int(date[8:10])
            h=hour # int(hour[0:2])
            minute = 0 # int(hour[3:5])
            dtime=pd.to_datetime(pd.DataFrame({'year': [year],'month': [month],'day': [day],'hour':[h],'minute':[minute]}))[0]
            dtime=dtime.to_datetime64()
            list_finale.append(dtime)
    return list_finale

def disag_param_pixel(dtime:np.datetime64,safran_data:xr.Dataset,
                      points:pd.DataFrame,xf:xr.DataArray,yf:xr.DataArray,
                      datatrain_mean:pd.Series,datatrain_std:pd.Series,
                      model,disaggregate:bool=True,
                      name_product:str='product',
                      x_safran:str='x',y_safran='y',
                      time_safran:str='time',
                      predictors:list=['ta','elevation','dta','ta_mean_day','ta_sd_day'],
                      dta:float=None):
    
    ''' disaggregation of tair at a given time from a supervised model 

    Parameters
    ----------
    dtime : np.datetime64
        date when disaggregate
    
    safran_data:xr.Dataset
        xarray dataset from .nc files containing raw data of product
    
    points : pd.DataFrame 
        DataFrame containing at least x, y coordinates in safran_crs and elevation 
    
    xf: xr.DataArray corresponding to coordinates longitude / x in safran_crs to interpolate 
    
    yf: xr.DataArray corresponding to coordinates latitude / y in safran_crs to interpolate
    
    datatrain_mean : pd.Series
        mean of predictors over training dataset used to train the supervised model
    
    datatrain_std : pd.Series
        mean of predictors over training dataset used to train the supervised model
    
    model:tf.keras.Model
        supervised and trained model
    
    disaggregate : bool
        if True : disaggregation using the supervides model else : only linear interpolation
    
    name_product : str
        name of the product in reanalysis .nc files 
    
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files   
    
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files 
        
    predictors : list 
        list of size p of attributes to predict (type str)
    
    dta : float 
        lapse rate 
        
    Returns
    -------
    pd.DataFrame : disaggregated product at dtime for each point location
        ind : int 
            unique id of each point
        date_hms : date-hour
            np.datetime64 format
        date : str 
            format YYYY-MM-DD
        hour :
            hour when disaggregated
        ta_d :
            product disaggregated if disaggregate == True  / linaearly interpolated if disaggregate == False
        ta : 
            product linearly interpolated 

    
    References
    ----------
        B. Sebbar et al., “Machine-Learning-Based Downscaling of Hourly ERA5-Land Air Temperature over Mountainous Regions”, Atmosphere, vol. 14, no. 4, p. 610, Mar. 2023, doi: 10.3390/atmos14040610.    '''
    hour=pd.to_datetime(dtime).hour
    
    day=pd.to_datetime(pd.to_datetime(dtime).strftime("%Y%m%d"))
    
    # not disaggregate 
    if disaggregate ==False :
        
        # linear interpolation at specific hour at point location 
        valday=safran_data.sel({time_safran:dtime})[name_product].load().interp({x_safran:xf,y_safran:yf},method="linear").rename("ta")
        
        totpoints=points.merge(valday.to_dataframe()[[x_safran,y_safran,'ta_d']], on=[x_safran,y_safran])
        
    else :
        day_range=pd.date_range(start=day, end=day+pd.Timedelta(23, "h"),freq='H')   
        
        ds=safran_data.sel({time_safran : day_range}).load()
        
        # spatially interpolated product 
        valday=ds[name_product].interp({x_safran:xf,y_safran:yf},method="linear")
        
        valdaymean=valday.mean(dim=[time_safran]).rename("ta_mean_day")
        
        valdaystd=valday.std(dim=[time_safran]).rename("ta_sd_day")
    
        # linear interpolation at specific hour at point location 
        val=ds.sel({time_safran:dtime})[name_product].interp({x_safran:xf,y_safran:yf},method="linear").rename("ta")

        # merge of xarrays
        valday=xr.merge([val,valdaystd,valdaymean])

        totpoints=points.merge(valday.to_dataframe()[[x_safran,y_safran,'ta','ta_sd_day','ta_mean_day']], on=[x_safran,y_safran])

        print("Linear interpol mean = "+ str(totpoints.ta.mean()))
        
        # evaluation of lapse rate  
        if dta==None :
            
            regressor = LinearRegression()
            
            regressor.fit(totpoints['elevation'].to_numpy().reshape(-1, 1) , totpoints['ta'].to_numpy().reshape(-1, 1))
            
            # learnig rate 
            dta=regressor.coef_[0][0]
            
        totpoints['dta']=min(dta,0)
    
        # prediction with model 
        X = totpoints[predictors].copy()
        
        # apply normalization techniques
        for column in X.columns:
            X[column] = (X[column] -datatrain_mean[column]) / datatrain_std[column] 
        
        X=X.to_numpy()
        
        # prediction 
        totpoints['ta_d'] = model.predict(X).ravel()
        
        print("NN predicted mean = "+ str(totpoints.ta_d.mean()))
    
    #totpoints['date_hms']=dtime.__str__()
    
    totpoints['date']=day
    
    totpoints['hour']=hour
    
    if disaggregate ==False :
        return totpoints[['ind','date','hour','ta']],day # add ta / 'date_hms',
    else :  
        return totpoints[['ind','date','hour','ta_d','ta']],day # add ta / 'date_hms',

def disaggregation_processing(idfeu:int, path2points:str,path2safran:str,
                             model_type:str,name_model:str,
                             listdate:list,start_hour:int,end_hour:int,
                             prefix_product:str,name_product:str,
                             x_safran:str,
                             y_safran:str,
                             time_safran:str,
                             safran_crs:int,
                             safran_res:float,
                             path2save:str,
                             point_file:str=None,
                             path2dem:str=None,
                             predictors:list=['ta','elevation','dta','ta_mean_day','ta_sd_day'],
                             disaggregate:bool=True,
                             dta_from_safran:bool=False):

    ''' disaggregation of tair 

    Parameters
    ----------
    idfeu : int
        unique id of studied area
        
    points_path:str 
        path to point file if .shp or to .gpkg (see get_points_location)
        
    model_type:str
        supervised model to train : 'nn' for neural network
        
    name_model : str
        name of the model 
    
    list_date : list 
        list of dates (type: str) ("YYYY-MM-DD") when disaggregrate 
        
    start_hour:int
        min hour to evaluate training data 
    
    end_hour:int
        max hour to evalute training data 
    
    name_product : str
        name of the product in reanalysis .nc files 
        
    x_safran : str
        name of the coordinate/dimension corresponding to longitude/x in .nc reanalysis files 
    
    y_safran : str
        name of the coordinate/dimension corresponding to latitude/y in .nc reanalysis files  
    
    time_safran:str
        name of the coordinate/dimension corresponding to time in .nc reanalysis files 
    
    safran_res:float
        resolution of the reanalysis grid in safran_crs unit 
        
    prefix_product : str 
        common prefix of reanalysis files containing all location 
        
    path2save:str
        folder to save results
        
    point_file:str
        None if points_path is .shp of name of layer if points_path is gpkg
        
    path2dem:str
        path to dem file (see get_points_topo)
        
    predictors : list 
        list of size p of attributes to predict (type str)
    
    disaggregate : bool
        if True : disaggregation using the supervides model else : only linear interpolation
        
    dta_from_safran:bool
        if true ude lapse rate from safran data to compute the disaggregation

        
    Returns
    -------
    None 
    Save in path2save .csv files named "ffid<idfeu>_ta_disag_<date>.csv (with date format YYYY-MM-DD)
    containing the following attributes 
        ind : int 
            unique id of each point
        date_hms : date-hour
            np.datetime64 format
        date : str 
            format YYYY-MM-DD
        hour :
            hour when disaggregated
        ta_d :
            product disaggregated if disaggregate == True  / linaearly interpolated if disaggregate == False
        ta : 
            product linearly interpolated if disaggregate == True / if disaggregate == False not present
    
    References
    ----------
        B. Sebbar et al., “Machine-Learning-Based Downscaling of Hourly ERA5-Land Air Temperature over Mountainous Regions”, Atmosphere, vol. 14, no. 4, p. 610, Mar. 2023, doi: 10.3390/atmos14040610.

    '''
    
    
    # loading points where disaggregating
    points,xf,yf=get_points_location(path2points,
                                     point_file=point_file,path2dem=path2dem,
                                     x_safran=x_safran,y_safran=y_safran,
                                     safran_crs=safran_crs)
    
    print("---> Study area grid points loaded")
    
    # train dataset mean/std 
    
    datatrain=dd.read_csv("ffid"+str(idfeu)+"_datatrain.csv")
    
    datatrain_mean=datatrain[predictors].mean().compute()
    
    datatrain_std=datatrain[predictors].std().compute()
    
    del datatrain
    
    # loadinf=g model  
    if model_type=='nn':
        
        model = keras.models.load_model(os.path.join(path2save,name_model))
        
        print("---> Model loaded")
    
    # hours when disaggregate 
    
    list_hour=[start_hour,end_hour]
    
    list_date_ts=get_date([start_hour,end_hour],listdate)
    
    # open safran data in parallel
    
    safran_data = xr.open_mfdataset(os.path.join(path2safran,prefix_product+"*.nc"), combine='by_coords',parallel=True)
    
    # selection of years 
    
    list_date_ts_ok=list(np.asarray(safran_data.isel({time_safran:safran_data.time.isin(list_date_ts)}).load()[time_safran]))
    
    print("---> Number of datetime :" ,len(list_date_ts_ok))
    
    # evluation of dta 
    if dta_from_safran==True :
        
        bbox=get_points_bbox(path2points,point_file=point_file)
        
        safran_select,x_select,y_select=select_safran_grid(path2safran,bbox,x_safran=x_safran,y_safran=y_safran,
                                                       buffer_distance=1.5*sqrt(2)*safran_res)  
        
        # if no elevation in safran select 
        if path2dem !=None :
            safran_select=get_points_topo(safran_select,path2dem)   
        
        safran_select=safran_select.loc[(safran_select["elevation"]>=-100)].copy()
      
    for dtime in list_date_ts_ok :
        
        print(dtime)
        
        # get sfaran dta 
        if dta_from_safran :
            
            datatrain_safran=get_safran_data(dtime,safran_select,safran_data.sel({x_safran:x_select,y_safran:y_select}),
                                             x_safran=x_safran,y_safran=y_safran,time_safran=time_safran,
                                             name_out='ta')
            
            dta=datatrain_safran['dta'].mean()
            
            print(datatrain_safran['dta'].mean())
            
        else :
            
            dta = None 
        
        totpoints,date=disag_param_pixel(dtime,
                                         safran_data,
                                         points,
                                         xf,yf,
                                         datatrain_mean=datatrain_mean,datatrain_std=datatrain_std,
                                         model=model,disaggregate=disaggregate,
                                         name_product=name_product,
                                         x_safran=x_safran,y_safran=y_safran,
                                         time_safran=time_safran,
                                         predictors=predictors,
                                         dta=dta)

        
        date=np.array([date],dtype='datetime64[D]')[0]
        
        # totpoints.set_index('ind',inplace=True)
        
        path2savefile=os.path.join(path2save,"ffid"+str(idfeu)+"_ta_disag_"+str(date)+".csv")
        
        if os.path.exists(path2savefile):
            #print('ok')
            totpoints.to_csv(path2savefile,mode="a",header=False,index=False)
        else :
            #print('ok')
            totpoints.to_csv(path2savefile,mode="w",header=True,index=False)
            
    return None


