# General package 
import os.path
import itertools
import glob

# data management package 
import numpy as np
import dask.dataframe as dd
import pandas as pd
import xarray as xr
import datetime

# spatial data management
import fiona
import geopandas as gpd
import rasterio
import pyproj
from shapely.geometry import box

# math function and statistical package 
from math import sqrt
import math
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from scipy.stats import linregress

# solar angles 
import pvlib

# plot 
import matplotlib.pyplot as plt 
