from packages import *

def split_window_average(df:pd.DataFrame,value:str,x:str="x",y:str="y",time:str="date", 
                         x_size:int=5,y_size:int=5,freq_min_periods:float=1):
    ''' split window average of (value) over (x,y) space for each (time) step  

    Parameters
    ----------
    df : pandas.DataFrame 
        contains at least x, y, time and value columns
    value : str
        value to smooth / name of attribute
    x : str 
        x-space name of equally spaced column
    y : str
        y-space name of  equally spaced column
    time : str 
        name of time column
    x_size :int 
        size in x direction of split window
    y_size : int
        size in y direction of split window
    freq_min_periods : float 
        min frequency of x_size*y_size to compute mean (else return Nan)
        
    Returns
    -------
    dataframe of smoothed values
    
    References
    ----------
    Victor PENOT
    '''
    df=df.set_index([x,y,time])
    df=xr.Dataset.from_dataframe(df[[value]]).chunk({x : 30,y :30})
    # améliorer car x et y doivent etre les nom des colonnes 
    df=df.rolling({x : x_size,y : y_size}, center=True,min_periods=int(x_size*y_size*freq_min_periods)).mean()
    return df.to_dataframe().reset_index()

def ndi(red:float,nir:float,ll=0):
    
    ''' compute normalized difference index

    Parameters
    ----------
    red : float 
        red band
        
    nir : float 
        nir band
        
    ll : float
        offset (see O.Hagolle)
        
    Returns
    -------
    ndvi : float
        
        
    References
    ----------
    Victor PENOT 
    '''
    return (nir-(red+ll))/(nir+red+ll)

def fvg_computation(rsdata:pd.DataFrame,sat:str,
                    ndvi_min:float=0,ndvi_max:float=1,qndvi_max:float=0.97,qndvi_min:float=0.01,
                    clear_soil_pixel:dict={"l7":5440,"l8":21824,"l9":21824},reflectance_quality_flag:str='qa_pixel'):
    
    ''' compute green fraction vegetation cover named vindex

    Parameters
    ----------
    rsdata:pd.DataFrame
        DataFrame containing at least ind (unique pixel/point id), red, nir, qa_pixel 
    
    ndvi_min : float
        mininimum value of ndvi
    
    ndvi_max : float
        maximum value of ndvi
        
    qndvi_max:float
        quantile of ndvi to evalute max value
    
    qndvi_min:float
        quantile of ndvi to evalute min value
        
    clear_soil_pixel : dict
        dictionnary of value of reflectance_quality_flag for clear water pixels for each satellite type (key)
 
    reflectance_quality_flag : str
        column name in df of reflectance quality flag 
    
        
    Returns
    -------
    vindex : float
        green fraction vegetation cover named vindex
        
        
    References
    ----------
    Victor PENOT 
    '''
    
    columns=rsdata.columns.to_list()+['vindex']
    
    rsdata['ndvi']=ndi(rsdata['red'],rsdata['nir'])
    
    ndvi_min=rsdata.loc[rsdata[reflectance_quality_flag].isin([clear_soil_pixel[sat]])]['ndvi'].quantile(qndvi_min)
    
    ndvi_max=rsdata.loc[rsdata[reflectance_quality_flag].isin([clear_soil_pixel[sat]])]['ndvi'].quantile(qndvi_max)
    
    rsdata['vindex']=np.square((rsdata['ndvi']-ndvi_min)/(ndvi_max-ndvi_min))
    
    rsdata['vindex']=np.where(rsdata['vindex']<0,0,rsdata['vindex'])
    
    rsdata['vindex']=np.where(rsdata['vindex']>1,1,rsdata['vindex'])
    
    return rsdata[columns]
