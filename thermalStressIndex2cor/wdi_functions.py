from packages import *
from remote_sensing_functions import *
from ta_disaggregation_functions import select_safran_grid


    
def get_shadow_correction_slope(rain_cumsum:pd.DataFrame, 
                                wdi_data:pd.DataFrame, 
                                zenith_data:pd.DataFrame,
                                max_month_calib:int=5,
                                min_month_calib:int=9,
                                max_year_calib:int=0,
                                min_year_calib:int=1e6,
                                max_date_calib:str='2100-01-01',
                                name_product:str='product',
                                time_safran:str='time',
                                wdi_name:str='wdi',
                                nmin_ind:int=3,
                                epsy:float=0,
                                nmin_calib:int=2):
    
    ''' Calibrate per pixel slope a_self of cast shadow correction

    Parameters
    ----------
    rain_cumsum : pd.DataFrame
        DataFrame of cumulative rain at least for each satellite overpass time. 
        Must contain name_product and time_safran (type datetime.datetime) attributes
    wdi_data : pd.DataFrame 
        Per pixel time series of wdi values. Must contain ind (unique pixel id), 
        wdi_name, and date (format YYYY-MM-DD) attributes
    zenith_data : pd.DataFrame 
        per pixel values of solar zenith angle [deg], at each satellite overpass time. 
        Must contain "ind", "zenith", and "date" attributes.
    max_month_calib : int
        maximum month number for calibration
    min_month_calib : int
        minimum month number for calibration
    max_year_calib : int
        maximum year value for calibration
    min_year_calib : int
        minimum year value for calibration
    max_date_calib :str
        date format YYYY-MM-DD max to calibrate 
    name_product : str
        name of the product in rain_cumsum 
    time_safran:str
        name of the coordinate/dimension corresponding to time rain_cumsum
    wdi_name : str 
        name of the wdi column in wdi_data 
    nmin_ind : int
        minimum number of very dry dates per ind to keep ind in the calibration process
    * args : see wdi_functions.get_upper_edge7


    Returns
    -------
    a_self : pd.DataFrame 
        ind : unique id of pixel
        a_self : self calibrated slope 
    wdi_data_very_dry :pd.DataFrame
        selected pixel, date and wdi_name value for calibration in wdi_data
        
    References
    ----------
    * V. Penot and O. Merlin, "Estimating the Water Deficit Index of a Mediterranean Holm Oak Forest From Landsat Optical/Thermal Data: A Phenomenological Correction for Trees Casting Shadow Effects," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, vol. 16, pp. 5867-5880, 2023, doi: 10.1109/JSTARS.2023.3288360.

    '''
    # first quartile of cumulataive rainfall 
    
    q25_rain_cumsum=rain_cumsum.dropna()[name_product].quantile(q=0.25)
    
    print("---> First quartile of cumulative rain : ",np.round(q25_rain_cumsum,3))
    

    print("# Selecting very dry dates  ... ")
    
    very_dry_dates=rain_cumsum.loc[(rain_cumsum[name_product]<=q25_rain_cumsum) &
                                   (rain_cumsum[time_safran].dt.month<=max_month_calib)&
                                   (rain_cumsum[time_safran].dt.month>=min_month_calib)&
                                   (rain_cumsum[time_safran].dt.year<=max_year_calib)&
                                   (rain_cumsum[time_safran].dt.year>=min_year_calib)&
                                   (rain_cumsum[time_safran]<pd.to_datetime(max_date_calib))
                                  ][time_safran].dt.strftime("%Y-%m-%d").unique()

    print("---> Number of very dry dates : "+str(len(very_dry_dates)))

    print("# Selecting pixels to callibrate  ... ")


    # select very dry dates
    wdi_data_very_dry=wdi_data.loc[wdi_data['date'].isin(very_dry_dates)]
    
    
    print("---> Number of very dry dates in rs data : "+str(len(wdi_data_very_dry['date'].unique().compute())))


    # remove nan values
    wdi_data_very_dry=wdi_data_very_dry.dropna(subset=[wdi_name])


    wdi_data_very_dry=wdi_data_very_dry.merge(zenith_data[["ind",'date','zenith']],on=['ind','date'])



    # select ind that only have 
    ind_ok=wdi_data_very_dry.groupby('ind').count().compute()[wdi_name]

    # select ind ok 
    ind_ok=ind_ok.loc[ind_ok>nmin_ind].index.to_list()

    print('---> Number of pixels to callibrate : '+str(len(ind_ok)))


    # select dry dates ok for ind
    wdi_data_very_dry=wdi_data_very_dry.loc[wdi_data_very_dry['ind'].isin(ind_ok)]

    # compute slope of correction a_self
    print("# Computing self callibration slope a_self ...")
    
    a_self=wdi_data_very_dry.set_index('ind').map_partitions(get_upper_edge,
                                                             x="zenith",
                                                             y=wdi_name,
                                                             epsy=epsy,
                                                             nmin=nmin_calib,
                                                             unique_id='ind',
                                                             meta=pd.Series(dtype="float64",name="a_self")
                                                            ).compute()
    a_self=a_self.to_frame().reset_index()
    
    
    return a_self,wdi_data_very_dry


    
def compute_wdi(rs_data:pd.DataFrame, points:pd.DataFrame, 
                sat:str,tair_mean:float,
                col_tir:str,window_size:int=3,window_size_tir:int=3,
                use_tir_topo_cor:bool=True,
                plot_space:bool=True,
                M:int=40,qt:int=0.9999) : 
    ''' compute water deficit index 

    Parameters
    ----------
     rs_data : pd.DataFrame
        DataFrame containing RS data for a given scene (date/sat/tile) : ind (unique point id), tir, nir, red, reflectance quality flag (qa_pixel), date, tile, sat
        Must be filtered on clear soil and water pixels 
        
    points : pd.DataFrame
        coordinates dataframe containing ind (unique points id), x_i and y_i coordinates of each points /!\ regular grid

    sat : str 
        satellite id
    
    tair_mean : float
        mean air temperatue in the scene [K]
    
    col_tir : str 
        name of the lst/tir attribute in rs_data
        
    window_size:int 
        size of the windows to smooth red, nir  value 
    
    window_size_tir:int 
        size of the windows to smooth lst/tir value /! only used if use_tir_topo_corr==False 
        because already taken into account in tir corrected from topographic effects

    use_tir_topo_cor:bool
        if True : use land surface temperature corrected from topographic effects else use tir not corrected

    M : int
        see get_dry_edge
    qt : float 
        see get_dry_edge
        
    Returns
    -------
    rs_data : pd.DataFrame 
        ind : int 
        sat : str
        tile : str
        date :str
        wdi : float
            water deficit index 
        wdi_s : float 
            water deficit index smooth with window size        
        
    References
    ----------
    * V. Penot and O. Merlin, "Estimating the Water Deficit Index of a Mediterranean Holm Oak Forest From Landsat Optical/Thermal Data: A Phenomenological Correction for Trees Casting Shadow Effects," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, vol. 16, pp. 5867-5880, 2023, doi: 10.1109/JSTARS.2023.3288360.
 
    '''
     ########################### SMOOTHING ################################
    
    if window_size>1 :
        
        # merge with points 
    
        rs_data=rs_data.merge(points[['ind','x_i','y_i']],on='ind')
    
        # smooth bands 
    
        smooth_red=split_window_average(rs_data[['x_i','y_i','red','date']],
                                        value='red',x="x_i",y="y_i",time="date",
                                        x_size=window_size,y_size=window_size)

        #smooth_red.rename(columns={'red':'red_s'},inplace=True)

        smooth_nir=split_window_average(rs_data[['x_i','y_i','nir','date']],
                                        value='nir',x="x_i",y="y_i",time="date",
                                        x_size=window_size,
                                        y_size=window_size)
        rs_data=rs_data.drop(columns=['red', 'nir'])
        
        rs_data=rs_data.merge(smooth_red[['x_i',"y_i",'red']],on=['x_i','y_i'])

        rs_data=rs_data.merge(smooth_nir[['x_i',"y_i",'nir']],on=['x_i','y_i'])
        
        del smooth_nir,smooth_red
        #smooth_nir.rename(columns={'nir':'nir_s'},inplace=True)
        
    if window_size_tir > 1 : 
        
        if use_tir_topo_cor==False:
            
            smooth_tir=split_window_average(rs_data[['x_i','y_i',col_tir,'date']],
                                            value=col_tir,x="x_i",y="y_i",time="date",
                                            x_size=window_size,
                                            y_size=window_size)

            rs_data=rs_data.drop(columns=[col_tir])

            rs_data=rs_data.merge(smooth_tir[['x_i',"y_i",col_tir]],on=['x_i','y_i'])

            del smooth_tir
    # print("---> RS data loaded")
    # compute ndvi and vindex
    rs_data=fvg_computation(rs_data,sat=sat)
    
    # remove no data in column 
    rs_data.dropna(subset=[col_tir,'vindex'],inplace=True)
    
    # evaluation of wdi 
    # wet edge temperature 
    tsvw=min(tair_mean,rs_data[col_tir].min())
    
    # get intercept and slope of tir-vindex upper linear egde
    
    bdry,adry=get_dry_edge(rs_data,y=col_tir,M=M,qt=qt,x='vindex')
    
    if plot_space :
        # plot tir/lst-vindex/fvg space and edges 
        plt.scatter(rs_data['vindex'],rs_data[col_tir])
        # dry edge 
        xline=np.array([0,1])
        yline=adry*xline+bdry
        plt.plot(xline,yline,'r',label='Dry edge',lw=2)
        # wet edge 
        plt.hlines(y=tsvw, xmin=0, xmax=1, colors='green', linestyles='-', lw=2, label='Wet edge')
        # plot attributes 
        plt.legend()
        plt.xlabel('fvg')
        plt.ylabel('Ts (K)')
        plt.title("Ts-fvg space - "+str(rs_data['date'].unique()[0]))
        plt.show()
    
    # compute wdi 
    
    rs_data['wdi']=(rs_data[col_tir]-tsvw)/(rs_data['vindex']*adry+bdry-tsvw)
    
    # set max and min values 
    
    cond_ddf=rs_data['wdi']>1

    rs_data['wdi'] = rs_data['wdi'].mask(cond=cond_ddf,other= 1)

    cond_ddf=rs_data['wdi']<0

    rs_data['wdi'] = rs_data['wdi'].mask(cond=cond_ddf,other= 0)
    
    #rs_data['minute']=minute
                                             
     
    
    # attributes to save 

    col2save=['wdi','ind','sat','tile','date']
    
    # smooth wdi
    if window_size > 1 :
        smooth_wdi=split_window_average(rs_data[['x_i','y_i','wdi','date']],
                                     value='wdi',x="x_i",y="y_i",time="date",x_size=5,y_size=5)

        smooth_wdi.rename(columns={'wdi':'wdi_s'},inplace=True)

        rs_data=rs_data.merge(smooth_wdi[['x_i',"y_i",'wdi_s']],on=['x_i','y_i'])
        
        col2save.append('wdi_s')
        
    return rs_data[col2save]

def get_dry_edge(data:pd.DataFrame,M:int =40,qt:float=0.99,x:str='vindex',y:str='tir'):
    ''' Calculates dry edge / upper edge  of (x,y) space 

    Parameters
    ----------
    data : pandas.DataFrame 
        contains at least x and y columns
    M : int
        number of intervals of equal number of values x 
    qt : float 
        quantile of y to regress over x
    x : str
        name of x column
    y : str 
        name of y column
        
    Returns
    -------
    (intercept,slope) of the upper edge 
    
    References
    ----------
    * V. Penot and O. Merlin, "Estimating the Water Deficit Index of a Mediterranean Holm Oak Forest From Landsat Optical/Thermal Data: A Phenomenological Correction for Trees Casting Shadow Effects," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, vol. 16, pp. 5867-5880, 2023, doi: 10.1109/JSTARS.2023.3288360.

    '''

    # split into equal length dataframe ordered by x
    data_split=np.array_split(data[[x,y]].sort_values(by=[x]), M)
    # get quantile of y and median of x by equal length dataframe
    # list of values to regress
    tir_qt=[]
    vindex_qt=[]
    for m in range(M):
        tir_qt.append(np.quantile(data_split[m][y],qt))
        vindex_qt.append(np.quantile(data_split[m][x],0.5))
    # linear regression of quantile y over x 
    regressor = LinearRegression().fit(np.array(vindex_qt).reshape(-1, 1),np.array(tir_qt).reshape(-1, 1))
    # slope of linear regression
    slope=regressor.coef_[0][0]
    # intercept of linear regression 
    intercept=regressor.intercept_[0]
    
    return intercept,slope 

"""
def covariance(x, y):
    return (
        (x - x.mean(axis=-1, keepdims=True)) * (y - y.mean(axis=-1, keepdims=True))
    ).mean(axis=-1)

def slope(x, y):
    return covariance(x, y) / (x.std(axis=-1) * x.std(axis=-1))
"""

def get_upper_edge(df:pd.DataFrame,x:str='zenith',y:str='wdi_s',unique_id:str='ind',
                   epsy:float=1e-2,epsa:float=5e-4,nmin:int=3,kmax:int=1000):
    ''' slope a_self of the linear upper edge of WDI-theta_S space   

    Parameters
    ----------
    df : pd.DataFrame / dd.DataFrame
        contains at least x,y, and unique_id column
    x: str
        x-axis of space / theta_S
    y : str
        y-axis of space / WDI
    unique_id : str
        unique id of points / ind 
    ncpu: int
        number of partitions for dask
    *args : see get_upper_edge_np

    Returns
    -------
    pd.Series of slope a_self / a computer
    
    References
    ----------
    Victor PENOT
    dd.from_pandas(,npartitions=ncpu)
    '''
    if df.index.name !=unique_id :
        print('/!\ set '+ unique_id + " as index !")
        return None
    # aself 
    return df.groupby(df.index,group_keys=False).apply(
        lambda df: get_upper_edge_dd(df[x],df[y],epsy=epsy,epsa=epsa,nmin=nmin,kmax=kmax))#,
        #meta= pd.Series(dtype="float64",name="a_self"))
    
    #aself.index = aself.index.rename(unique_id)
    
    #aself=aself.to_frame()
    
    #return aself

def get_upper_edge_dd(x:pd.Series,y:pd.Series,epsy:float=1e-4,epsa:float=5e-4,nmin:int=3,kmax:int=1000):
    ''' slope a_self of the linear upper edge of WDI-theta_S space   

    Parameters
    ----------
    x : pandas.Series  
        series of theta_s
    y : pandas.Series  
        series of wdi
    *args : see get_upper_edge_np

    Returns
    -------
    pd.Series of slope a_self
    
    References
    ----------
    Victor PENOT
    '''
    return get_upper_edge_np(np.asarray(x).flatten(),np.asarray(y).flatten(),epsy=epsy,epsa=epsa,nmin=nmin,kmax=kmax)

def get_upper_edge_np(x:np.array,y:np.array,epsy:float=1e-2,epsa:float=5e-4,nmin:int=3,kmax:int=1000):
    ''' slope a_self of the linear upper edge of WDI-theta_S space  for one spatial place

    Parameters
    ----------
    x : np.array
        array of theta_s
    y : np.array
        array of wdi
    unique_id : str
        unique id of points / ind 
    epsy : float = 1e-4
        epsilon along y axis to put equal two y values
    epsa : float = 5e-4
        step of slope 
    nmin : int = 3
        minimum number of points for the upper edge 
    kmax : int = 1000
        maximum number of iterations to find the slope

    Returns
    -------
    float / slope a_self
    
    References
    ----------
    Victor PENOT
    '''
    
    if len(x)<2:
        
        return 0
    
    y_max=y.max(axis=-1)
    
    # print(y_max)
    
    i_max=y.shape[-1] - 1 - np.argmax(np.flip(y, axis=-1), axis=-1)

    if i_max==len(x)-1:
        return 0
    else :
        x_max=x[i_max]
        
    # print(x_max)
        
    # get values near ymax and x over x_max
    
    x_datamax=x[np.where(((y-(y_max-epsy))>=0),True,False)]#& ((x-x_max)>=0)
    
    y_datamax=y[np.where(((y-(y_max-epsy))>=0),True,False)]#& ((x-x_max)>=0
    
    #print(x_datamax)
    
    n=len(x_datamax)
    
    #print(n)
    
    #print(x)
    
    #print(x_datamax)
    
    if n>=nmin :
        
        return 0
    
    else :
        
        x_max=np.max(x_datamax)
        
        #print(x_max)
        
        y=y[(~np.isin(y,y_datamax)) & np.where((x-x_max)>=0,True,False)]
        
        x=x[(~np.isin(x,x_datamax)) & np.where((x-x_max)>=0,True,False)]
    
    # initalization of slope and counter
    a0=0
    k=0
    out_loop=False
    
    # Main loop 
    while k<=kmax:

        # actualisation of slope
        
        a=a0-epsa
        
        b=y_max-a*x_max
        
        # number of selected points (+1 because x_max,y_max rm in y)
        
        n=len(y[np.where(y>=a*x+b-epsy)])+1
        
        # actualisation of seps variables 
        
        a0=a
        
        k+=1
        
        if np.sign(a)*a>10 :
            k=kmax+1
            out_loop=True
        elif k>kmax:
            out_loop=True
        elif n>=nmin :
            k=kmax+1
        elif n==len(x[np.where(x>=x_max)]):
            k=kmax+1
            out_loop=True
            
    if out_loop==True :
        return 0
    else :
        y_selected=y[np.where(y>=a*x+b-epsy)]
        y_datamax=np.concatenate((y_max,y_selected), axis=None)
        x_datamax=np.concatenate((x_max,x[np.where(np.isin(y,y_selected))]), axis=None)
        return linregress(x_datamax, y_datamax).slope#a #slope(x_datamax, y_datamax)




