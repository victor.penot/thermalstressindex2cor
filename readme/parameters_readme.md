
# Description of the parameters in thermalStressIndex2cor/parameters.py

## Working directory and name of the project 

```path2wd```  *str* : path to the folder containing the project.

```name```  *str* : name of the project. Please provide a name without spaces, special characters/symbols, etc.

```idfeu```  *int* : number of the study area in the project

## CPU units

```ncpu```  *int*  : number of CPU units 

## Remote sensing parameters 

```clear_soil_pixel``` *dict* : value of quality reflectance mask for clear soil pixels and each satellite product. The key is the satellite id. For instance USGS provides reflectance quality masks named "qa_pixel" : for the Landsat-7 (key ```l7```) satellite mission, the value 5440 corresponds to clear soil pixel, for Landsat8/9 (keys ```l8``` and ```l9``` respectively) satellite mission it is 21824. More information about USGS Landsat quality mask values - Level 2 products are available [here](https://www.usgs.gov/landsat-missions/landsat-collection-2-quality-assessment-bands)


```clear_water_pixel``` *dict* : value of quality reflectance mask for clear water pixels and each satellite product. Same description as previous item.


```fill_no_data``` *dict* : value of quality reflectance mask for no data pixel and each satellite product. Same description as previous item.

```start_hour``` *int* : lower bound of the interval of satellite overpass time.

```end_hour``` *int* : upper bound of the interval of satellite overpass time.

Note that $[10;11]$ corresponds to the Landsat-7/8/9 UTC time overpass. These values are of huge interest for the correction of cast shadow effect.


```window_size``` *int* : size of the smoothing (mean) split window of the VIS/NIR channels.

```window_size_tir``` *int* : size of the smoothing (mean) split window of the TIR channels.

Landsat TIR channels have a native spatial resolution of 100m (Landsat-8/9) and 60m (Landsat-7). They are resampled and provided at 30m spatial resolution (same as VIS/NIR channels) by USGS (Level-2 product). To avoid artefacts in the contextual method used to compute the WDI, a smoothing step is realized prior to compute this index or to use the associated space (Land Surface Temperature - Fractional green vegetation cover).


## Meteorological reanalysis data parameters 

```safran_res``` *float* : spatial resolution of the meteo grid. Note that this value must be expressed in the remote sensing CRS unit. For instance, 8000 meters corresponds to the French SAFRAN reanalysis dataset in WGS84-UTM31 or WGS84-UTM32 unit (Landsat tiles of Southern-East France).

```time_safran``` *str* : name of the time coordinate in *.nc* files. For instance it is *time* in SAFRAN dataset.

```x_safran``` *str* : name of the lon/x coordinate in  *.nc* files. For instance it is *x* in SAFRAN dataset.

```y_safran``` *str* : name of the lat/y coordinate in  *.nc* files. For instance it is *x* in SAFRAN dataset.

Note that lon/lat or x/y coordinates in *.nc* files, must be expressed in the same crs than the ```safran_gridpoints.shp``` one.

### Air temperature 

```prefix_product_ta``` *str* : prefix of all *.nc* meteo (reanalysis) files corresponding to near-surface 2m air temperature. For instance it is *ForcT* in the SAFRAN dataset.

```name_product_ta``` *str* : name of the air temperature product (dataset) in the meteo *.nc* file. For instance it is *product* in the SAFRAN dataset.

### Wind speed 

```prefix_product_ws``` *str* : prefix of all *.nc* meteo (reanalysis) files corresponding to horizontal wind speed. For instance it is *ForcVu* in the SAFRAN dataset.

```name_product_ws``` *str* : name of the horizontal wind speed product (dataset) in the meteo *.nc* file. For instance it is *product* in the SAFRAN dataset.

### Specific humidity  

```prefix_product_q``` *str* : prefix of all *.nc* meteo (reanalysis) files corresponding to specific humidity. For instance it is *ForcQ* in the SAFRAN dataset.

```name_product_q``` *str* : name of the specific humidity product (dataset) in the meteo *.nc* file. For instance it is *product* in the SAFRAN dataset.

### Rain / Precipitation rate 

```prefix_product_r``` *str* : prefix of all *.nc* meteo (reanalysis) files corresponding to rain. For instance it is *ForcQ* in the SAFRAN dataset.

```name_product_r``` *str* : name of the rain product (dataset) in the meteo *.nc* file. For instance it is *product* in the SAFRAN dataset.

```fact_rain``` *int* : multiplicative factor to express rain in $mm.h^{-1}$. Natively SAFRAN dataset is provided in $kg.m^{-2}.s^{-1}$

## Air temperature downscalling step 

```model_type``` *str* $\in$ ['nn'] : name of the machine learning model used to downscale the air temperature among : 'nn' for Neural Network.

```prefix_sta``` *str* : Prefix of all ground air temperature *.csv* files

``` unique_id_sta ``` *str* : name of the column of unique id of each ground station in *.csv* files

```sta_name_product``` *str* : name of the air temperature column in *.csv* files

```time_sta``` *str* : name of the date-time column in *.csv* files

```z_sta``` *str* : name of the elevation column in *.csv* files

## Date and time selection

```min_month``` *int* $\in [1,12]$ : lower month of the year to correct land surface temperature for topographic effect and WDI for cast shadow.

```max_month``` *int* $\in [1,12]$ : upper month of the year to correct land surface temperature for topographic effect and WDI for cast shadow.

```freq_min_pix``` *float* $\in [0,1]$: minimum frequency of clear remotely sensed soil/water pixels in the study area. A value of 0.85 is used in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795). 


```dtemp_min``` *float* : maximum difference of temperature expressed in Kelvin, between the mean value of air temperature $T_{air}$ and the minimum land surface temperature $min(T_S)$ measured in the study area at a given date. Scenes/Dates such as $T_{air}-min(T_S) \ge$```dtemp_min``` are not selected. A value of 1K is used in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795).

## Land surface temperature correction for topographic effects

```eb_params``` *dict* : overall energy balance parameters. For each dictionnary used in ```eb_params```  the keys are as follows : 0 : bare soil / 1 : full vegetation cover.

```kb``` *dict* of *float*  : $Kb^{-1}$  parameter [-]

```albedo``` *dict* of *float* $\in [0,1]$  : albedo [-]

```emi``` *dict* of *float* $\in [0,1]$ :  emissivity [-]
   
```rs``` *float* :  stomatal resistance  [$s.m^{-1}$]

```h``` *float* : vegetation height [$m$]

```z``` *float* :  wind speed measurement heigth (from meteo reanalysis files) [$m$]

```cg``` *float* $\in [0;1]$ : frequency of net flux stored in soil [-]

```karman``` *float* = 0.41, Von Karmann constant [-]

```rcp``` *float* = 1262 Volumetric mass (density) of air  * specific heat of air [$J.K^{-1}.m^{-3}$]

```sigma``` *float* = 5.67*10**(-8) : Stephan-Boltzmann constant [$J.s^{-1}.m^{-2}.K^{-4}$]

```grav``` *float* =9.81 acceleration of gravity [$m.s^{-1}$]

```gam``` *float* = 67 psychometric constant [$Pa.K^{-1}$]

```fvdiff_solveg``` *float* $\in [0,1]$ : optimization parameter 


## WDI correction for cast shadow effects 

```use_tir_topo_cor``` *bool* : use of land surface temperature corrected from topographic effect

```shadow_correction``` *bool* : wdi cast shadow correction

### $a_{self}$ self-calibration parameters 

```rain_timelag``` *int* : cumulative rain timelag [days]. A value of 15 is used in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795).

```min_month_calib``` *int* $\in [1,12]$ : earliest month used for self-calibration. A value of 5 is used in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795).

```max_month_calib``` *int*  $\in [1,12]$ : lattest month used for self-calibration. A value of 9 is used in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795).


### $b_{self}$ computing method 

```b_self_method``` *str* $\in$ ['sat_view_zenith','min_solar_zenith','fixed'] : method used to set $b_{self}$ value.

  * if ```b_self_method```=='sat_view_zenith', file 'rs_data/ffid<idfeu>_satellite_viewing_angles.tif' must be provided
  
  * if ```b_self_method```=='min_solar_zenith', the mean of the minimum solar zenith angle observed in the study area is used for all pixels.
  
  * if ```b_self_method```=='fixed', you must provide ```b_self``` value (see below)

```b_self``` *float* = None : fixed value of $b_{self}$ (if ```b_self_method```=='fixed'). Set to ```None``` if not used.

``` fact_zenith``` *float* = 0.01 : zenith angle multiplicative parameter (for landsat 8, see [documentation](https://www.usgs.gov/landsat-missions/solar-illumination-and-sensor-viewing-angle-coefficient-files) )













