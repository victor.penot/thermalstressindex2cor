# thermalStressIndex2cor

## Synopsis

This project contains a *Python* code to estimate the hydric status (a stress index SI varying between 0 and 1) of vegetated land surfaces from shortwave and thermal infrared remote sensing data. It relies on auxiliary meteorological data and a digital elevation model to correct the SI estimates obtained by remote sensing for 1) topography and 2) possible shading effects of trees at the resolution of the thermal sensor. It has been originally built to deal with high resolution satellite data (Lansat-7/8/9) to estimate hydric stress by means of contextual methods (*e.g.* *Ts-fvg*)


This is an orderly three-step process, based on scientific references :

1. **Spatialization of air temperature** at satellite resolution as in [Sebbar, 2023](https://www.mdpi.com/2073-4433/14/4/610). 
/!\ The code is being optimised -- please do not download until the changes are complete.
    * **Inputs** : 
        * point grid of satellite pixel centroids in the study area  
        * 2-m air temperature *in situ* measurements within/near the study area with hourly timestep. It should be noted that, for optimum correction of elevation effects, the difference in elevation between the highest and lowest points of the measurements must be at least 800 m, and at least three weather stations must be available.;
        * air temperature from reanalysis meteorological dataset available at coarse spatial resolution (*e.g.* 0.25° ERA5 or 8km SAFRAN) to be spatialized at the thermal sensor resolution. Note that they must be provided on an hourly basis for the entire corresponding day for 1/  each available ground measurement and 2/ for each satellite date-time overpass. Ground measurement dates and satellites overpass dates are not necessarily the same.
        * elevation data at satellite resolution .
      
    * **Outputs** :
        * air temperature spatialized at the thermal sensor resolution (point grid of satellite pixel centroids) for each satellite overpass date.
      
    * **Principle** :
    
      A Machine learning model (here a Dense Neural Network or XgBoost (to be implemented)) is first trained to spatialize at the thermal sensor resolution the coarse resolution reanalysis dataset using in situ 2-m air temperature measurements and elevation raster. Then the model is used to predict spatialized air temperature from reanalysis data at the date-time of satellite overpass.
        
    * **What it does take into account** :
       * Real-time measured Environmental Lapse Rate at the date-hour satellite overpass. It is considered homogeneous in the study area.
      
    * **What it does not take into account** :
       * surface-atmosphere feedbacks, advection effects.
      

2. **Correction / normalization of surface temperature for topographic effects** [Malbetau, 2017](https://www.sciencedirect.com/science/article/abs/pii/S0034425716304412). 

    * **Inputs** : 
       * point grid of satellite pixel centroids in the study area  
       * air temperature previously spatialized at satellite resolution ;
       * elevation, slope and aspect data at satellite resolution ; 
       * wind speed and relative air humidity from reanalysis meteorological dataset. Currently these inputs are linearly interpolated at satellite resolution ; 
       * land surface temperature to be normalized, red and near infrared bands from remotely sensed data. 


    * **Outputs** :
       * files of corrected/normalized surface temperature.

    * **Principle** :
    
      Short-wave (solar) incoming radiation is estimated at satellite resolution. It is used with spatialized air temperature  to  normalize remotely sensed surface temperature to take into account the effects of variability in incoming solar radiation and air temperature variations induced by topography. 

    * **What it does take into account** :
       * incoming solar radiation at the thermal sensor resolution from the exposition of each pixel 
       * spatialized air temperature 

    * **What it does not take into account** :
       * disaggregated wind speed and relative/specific humidity ;
       * the diffuse and multiple reflexion components of solar radiation

3. **Correction of estimated hydric stress for cast shadow effects** [Penot, 2023](https://ieeexplore.ieee.org/document/10158795). 

    * **Inputs** :
       * point grid of satellite pixel centroids in the study area  
       * surface temperature corrected for topographic effects;
       * red and near infrared bands (to compute vegetation indice) from remotely sensed data;
       * previously spatialized high resolution (satellite) air temperature ;
       * rain from reanalysis meteorological dataset (15-day length before each satellite overpass);
       * satellite zenith viewing angle data at satellite resolution.
      
      It should be noted that 5 to 6 years of remote sensing data records should be used, particularly during summer/dry periods, to derive a robust correction for cast shadow effects.

    * **Outputs** :
       *  non-corrected and corrected estimated hydric stress (water stress index) for cast shadow effects at each satellite time overpass. 

    * **Principle** :
    
     The decrease of non corrected water stress index during dry period may find its roots in cast shadow effects that increase with solar zenith angle. A self-calibrated correction of this effect is done at the pixel scale, using times series of water stress index. 

    * **What it does take into account** :
        * phenomenological variations of water stress index with solar zenith angle that reflects the 3D canopy geometry / solar illumination interaction during drought/dry periods.
      
     * **What it does not take into account** :
       * Explicit modeling of 3D canopy with physical corrections
        
**Planned improvements** 

   * Physically-based spatialization of wind speed at satellite resolution;
   * Physically-based spatialization of solar radiation (including diffuse and multiple reflection radiation components) at satellite resolution;  
   * Adding evapotranspiration as an  output variable of the code;
   * Evaluating the performance of the code in a range of surface and atmospheric conditions.

  
  

## Installation

Currently, you just need to download the thermalStressIndex2cor folder containing all the basic functions that carry on downsampling and corrections. In a near future a friendly package will be built and provided.

## Package requirement 

The following Python libraries are required :

For tables :

Basic packages :

* itertools
* datetime
* glob

Table management : 

* numpy
* pandas 
* dask.dataframe
* xarray

Modelling and mathematics :

* scikit-learn
* sklearn
* scipy
* math 

Geograhic data management :

* geopandas 
* fiona
* shapely 
* rasterio 
* pyproj

Solar angles :

* pvlib

Currently, only a neural network model is available to downsample air temperature. Then you must also install :

* [keras](https://keras.io/)

## Mandatory data 

A description of the mandatory data and their organization is provided in the readme/input_data_specifications file.

## Setting parameters in ```parameters.py``` files

First you must fill the ```parameters.py``` file prior to use the functions/Notebooks. The description is provided in the readme/parameters_readme.md file.

## Code examples 

Three notebooks are provided in the notebooks folder :

* E0_ta_disaggregation.ipynb : it carries out air temperature downscalling as in [Sebbar, 2023](https://www.mdpi.com/2073-4433/14/4/610);
* E1_ts_topo_correction.ipynb : it carries out land surface temperature correction for topographic effects as in [Malbetau, 2017](https://www.sciencedirect.com/science/article/abs/pii/S0034425716304412) ; 
* E2_wdi_shadow_correction.ipynb : it computes Water Deficit Index and carries out correction for cast shadow effect as in [Penot, 2023](https://ieeexplore.ieee.org/document/10158795).

**NB** : Examples are built from : 

* Landsat-7/8/9 remote sensing data downloaded from [usgs-earthexplorer](https://earthexplorer.usgs.gov/). 

* Meteorological ground data come from a free dataset from MeteoFrance : [Meteonet](https://meteonet.umr-cnrm.fr/). 

* Reanalysis data come from [SAFRAN dataset](https://hal.science/meteo-00420845/).  

* Elevation data were downloaded from [Institut Geographique National](https://geoservices.ign.fr/bdalti). 
 

## Main Scientific References

* B. Sebbar et al., “Machine-Learning-Based Downscaling of Hourly ERA5-Land Air Temperature over Mountainous Regions,” Atmosphere, vol. 14, no. 4, p. 610, Mar. 2023, doi: 10.3390/atmos14040610.

* Y. Malbeteau, O. Merlin, S. Gascoin,J.-P. Gastellu-Etchegorry, C. Mattar, L. Olivera-Guerra, S. Khabba, L. Jarlan,"Correcting land surface temperature data for elevation and illumination effects in mountainous areas: A case study using ASTER data over a steep-sided valley in Morocco", in Remote Sensing of Environment, vol. 189. 25-39, 2017, 10.1016/j.rse.2016.11.010. 

* V. Penot and O. Merlin, "Estimating the Water Deficit Index of a Mediterranean Holm Oak Forest From Landsat Optical/Thermal Data: A Phenomenological Correction for Trees Casting Shadow Effects," in IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, vol. 16, pp. 5867-5880, 2023, doi: 10.1109/JSTARS.2023.3288360.

## Contributors

* Victor Penot : victor.penot@iut-tlse3.fr main developer

* Marine Bouchet : Land surface temperature algorithm - developper

* Olivier Merlin : scientific advisor 

* Badr-eddine Sebbar : scientific advisor 



